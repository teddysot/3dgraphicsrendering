#include "GameApp.h"
#include <windows.h>
using namespace Engine;

namespace //Global stuff only used in this cpp file
{

}

GameApp::GameApp()
{
}

GameApp::~GameApp()
{
}

void GameApp::OnInitialize()
{
	mWindow.Initialize(GetInstance(), GetAppName(), 1270, 720);
	Graphics::GraphicsSystem::StaticInitialize(mWindow.GetWindowHandle(), false);

	Input::InputSystem::StaticInitialize(mWindow.GetWindowHandle());

	mConstantBuffer.Initialize();
	mConstantBufferS.Initialize();

	mConstantBufferB.Initialize();

	mSphereVertexShader.Initialize(L"../Assets/Shaders/Texturing.fx", Graphics::VertexS::Format);
	mSpherePixelShader.Initialize(L"../Assets/Shaders/Texturing.fx");
	mCamera.SetPosition(Math::Vector3(0.0f, 20.0f, -400.0f));
	mCamera.SetDirection(Math::Vector3(0.0f, 0.0f, 1.0f));






	mLightVertexShader.Initialize(L"../Assets/Shaders/PhongReflection.fx", Graphics::VertexPNT::Format);
	mLightPixelShader.Initialize(L"../Assets/Shaders/PhongReflection.fx");





	// Skydome
	/*----------------------------------------------------------------------------------------------------------------*/
	mSkyDomeBuilder.GenerateSkydomeS(mSkyDome, 50, 50);
	mSkyDomeBuffer.Initialize(mSkyDome);
	mSkyDomeTexture.Initialize("../Assets/Images/galactic.jpg");
	/*----------------------------------------------------------------------------------------------------------------*/

	// Sun
	/*----------------------------------------------------------------------------------------------------------------*/
	mSunBuilder.GenerateSpherePNT(mSun, 50, 50);
	mSunBuffer.Initialize(mSun);
	mSunTexture.Initialize("../Assets/Images/8k_sun.jpg");
	/*----------------------------------------------------------------------------------------------------------------*/


}

void GameApp::OnTerminate()
{
	mWindow.Terminate();
	Graphics::GraphicsSystem::StaticTerminate();
	mConstantBuffer.Terminate();

	mSpherePixelShader.Terminate();
	mSphereVertexShader.Terminate();

	mSkyDome.Destroy();
	mSkyDomeTexture.Terminate();
	mSkyDomeBuffer.Terminate();

	mSun.Destroy();
	mSunTexture.Terminate();
	mSunBuffer.Terminate();

	mLightVertexShader.Terminate();
	mConstantBufferS.Terminate();
	mConstantBufferB.Terminate();
}

void GameApp::OnUpdate()
{
	if (mWindow.ProcessMessage())
	{
		Kill();
		return;
	}

	if (GetAsyncKeyState(VK_ESCAPE))
	{
		PostQuitMessage(0);
		return;
	}

	Graphics::GraphicsSystem::Get()->BeginRender();

	float speed = 0.0f;
	float cameraMoveSpeed = 3.0f;
	float cameraRotationSpeed = 0.05f;
	float cameraSpeedValue = 2.0f;

	if (GetAsyncKeyState(VK_SPACE)) // speed increase
	{
		speed = 40;
		cameraMoveSpeed = 20;
	}

	if (GetAsyncKeyState(VK_LEFT))
	{
		rotationY = rotationY + 0.1;
	}

	else if (GetAsyncKeyState(VK_RIGHT))
	{
		rotationY = rotationY - 0.1;
	}

	if (GetAsyncKeyState(VK_UP))
	{
		rotationX = rotationX + 0.1;
	}

	else if (GetAsyncKeyState(VK_DOWN))
	{
		rotationX = rotationX - 0.1;
	}

	if (GetAsyncKeyState('P') & 1) // play
	{
		Switch = !Switch;
	}

	if (Switch == true)
	{
		rotationY = rotationY - 0.01;
	}

	if (GetAsyncKeyState('O') & 1) // play
	{
		Switch1 = !Switch1;
	}

	if (Switch1 == true)
	{
		alpha1 = alpha1 + 1;
		alpha2 = alpha2 + 0.9;
		alpha3 = alpha3 + 0.86;
		alpha4 = alpha4 + 0.84;
		alpha5 = alpha5 + 0.77;
		alpha6 = alpha6 + 0.72;
		alpha7 = alpha7 + 0.68;
		alpha8 = alpha8 + 0.63;
		alpha9 = alpha9 + 0.59;
	}

	if (GetAsyncKeyState('L') & 1) // play
	{
		Switch2 = !Switch2;
	}

	if (Switch2 == true)
	{
		mCamera.SetLookAt(Math::Vector3(1.0f, 0.0f, 0.0f));
	}

	Input::InputSystem* is = Input::InputSystem::Get();
	is->Update();

	if (GetAsyncKeyState(VK_RBUTTON)) // right click to move
	{
		mCamera.Pitch((is->GetMouseMoveY() * cameraRotationSpeed * 0.05f)); // up down
		mCamera.Yaw((is->GetMouseMoveX() * cameraRotationSpeed * 0.05)); // left right
	}

	Graphics::GraphicsSystem* gs = Graphics::GraphicsSystem::Get();

	Math::Matrix viewMatrix = mCamera.GetViewMatrix();
	Math::Matrix projectionMatrix = mCamera.GetPerspectiveMatrix(gs->GetAspectRatio());

	Math::Matrix viewMatrix1 = Math::Inverse(mCamera.GetWorldMatrix());


	mCamera.SetFOV(FOV);

	if (GetAsyncKeyState('M')) // zoom out out to the furtest view
	{
		if (FOV <= 2.5f)
		{
			FOV = FOV + 0.05f;
		}

	}

	if (GetAsyncKeyState('N')) // zoom in to the nearest view
	{
		if (FOV >= 0.0f)
		{
			FOV = FOV - 0.05f;
		}
	}

	if (GetAsyncKeyState('B')) // reset the view to 1
	{

		FOV = 1.0f;
	}

	Math::Vector3 movement;

	if (GetAsyncKeyState('W'))
	{
		movement.z = cameraMoveSpeed + speed;
	}

	if (GetAsyncKeyState('S'))
	{
		movement.z = -cameraMoveSpeed - speed;
	}

	if (GetAsyncKeyState('D'))
	{
		movement.x = cameraMoveSpeed + speed;
	}

	if (GetAsyncKeyState('A'))
	{
		movement.x = -cameraMoveSpeed - speed;
	}

	if (GetAsyncKeyState('E'))
	{
		mCamera.Rise(cameraMoveSpeed * 0.1f);
	}

	if (GetAsyncKeyState('Q'))
	{
		mCamera.Fall(cameraMoveSpeed * 0.1f);
	}



	mCamera.Walk(movement.z * 0.1f); // forward backward
	mCamera.Strafe(movement.x * 0.1f); // left right




	mSphereVertexShader.Bind();
	mSpherePixelShader.Bind();





	// Skydome
	/*----------------------------------------------------------------------------------------------------------------*/

	mSkyDomeTexture.BindPS(0);

	Math::Matrix TRANSLATION;
	Math::Matrix WORLD;
	Math::Matrix SCALE;
	Math::Matrix Matrices[3];

	TRANSLATION = Math::Matrix::Translation(0, 0, 0);
	SCALE = Math::Matrix::Scaling(5000.0f);
	WORLD = TRANSLATION * SCALE;

	Matrices[0] = Math::Transpose(WORLD);
	Matrices[1] = Math::Transpose(viewMatrix);
	Matrices[2] = Math::Transpose(projectionMatrix);
	mConstantBuffer.Set(Matrices);
	mConstantBuffer.BindVS();

	mSkyDomeBuffer.Draw();

	/*----------------------------------------------------------------------------------------------------------------*/

	// Sun
	/*----------------------------------------------------------------------------------------------------------------*/


	float OX = cos(alpha1 * Math::kDegToRad);
	float OZ = sin(alpha1 * Math::kDegToRad);


	mLight.direction = Math::Normalize(Math::Vector3(OX, -1.0f, OZ));
	mLight.ambient = Math::Vector4(0.1f, 0.1f, 0.1f, 1.0f);
	mLight.diffuse = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	mLight.specular = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);

	mMaterial.ambient = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	mMaterial.diffuse = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	mMaterial.specular = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	mMaterial.power = 50.0f;




	mLightVertexShader.Bind();
	mLightPixelShader.Bind();
	mSunTexture.BindPS(0);

	Math::Matrix TRANSLATION_0;
	Math::Matrix WORLD_0;
	Math::Matrix SCALE_0;

	TRANSLATION_0 = Math::Matrix::Translation(1.2, 0, 0);
	SCALE_0 = Math::Matrix::Scaling(100.0f);
	WORLD_0 = Math::Matrix::RotationY(rotationY) * Math::Matrix::RotationX(rotationX) * TRANSLATION_0 * SCALE_0;

	ConstantData data;
	data.wvp = Math::Transpose(WORLD_0 * viewMatrix * projectionMatrix);
	data.World = Math::Transpose(WORLD_0);
	data.viewPosition = mCamera.GetPosition();
	data.light = mLight;
	data.material = mMaterial;
	mConstantBufferS.Set(data);
	mConstantBufferS.BindVS();
	mConstantBufferS.BindPS();
	mSunBuffer.Draw();

	/*----------------------------------------------------------------------------------------------------------------*/
	// Sun
	/*----------------------------------------------------------------------------------------------------------------*/


	float OX1 = cos(alpha2 * Math::kDegToRad);
	float OZ1 = sin(alpha2 * Math::kDegToRad);


	mLight.direction = Math::Normalize(Math::Vector3(OX, -1.0f, OZ));
	mLight.ambient = Math::Vector4(0.1f, 0.1f, 0.1f, 1.0f);
	mLight.diffuse = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	mLight.specular = Math::Vector4(0.0f, 0.0f, 0.0f, 1.0f);

	mMaterial.ambient = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	mMaterial.diffuse = Math::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	mMaterial.specular = Math::Vector4(0.0f, 0.0f, 0.0f, 1.0f);
	mMaterial.power = 50.0f;




	mLightVertexShader.Bind();
	mLightPixelShader.Bind();
	mSunTexture.BindPS(0);

	Math::Matrix TRANSLATION_1;
	Math::Matrix WORLD_1;
	Math::Matrix SCALE_1;


	TRANSLATION_1 = Math::Matrix::Translation(-1.2, 0, 0);
	SCALE_1 = Math::Matrix::Scaling(100.0f);
	WORLD_1 = Math::Matrix::RotationY(rotationY) * Math::Matrix::RotationX(rotationX) * TRANSLATION_1 * SCALE_1;

	ConstantData1 data1;
	data1.wvp1 = Math::Transpose(WORLD_1 * viewMatrix * projectionMatrix);
	data1.World1 = Math::Transpose(WORLD_1);
	data1.viewPosition1 = mCamera.GetPosition();
	data1.light1 = mLight;
	data1.material1 = mMaterial;
	mConstantBufferB.Set(data1);
	mConstantBufferB.BindVS();
	mConstantBufferB.BindPS();
	mSunBuffer.Draw();


	alpha1 = alpha1 + 1.0f;
	alpha2 = alpha2 + 1.0f;

	/*----------------------------------------------------------------------------------------------------------------*/

	Graphics::GraphicsSystem::Get()->EndRender();
}
