#include "GameApp.h"

using namespace Engine;

// nameless namespace & static ---> internal linkage


// vertices - unique point vertex array
// indices - pick 3 points out of time to draw a triangle

namespace //Global stuff only used in this cpp file
{
	// Define our triangle vertices
	const Graphics::VertexPC sVertices[] =
	{

		{ { -0.25f, -0.5f, 0.0f } ,{ 1.0f, 1.0f, 1.0f, 1.0f }},
		{ { -0.25f,  0.0f, 0.0f } ,{ 1.0f, 1.0f, 1.0f, 0.0f }},
		{ { 0.25f,  -0.5f, 0.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f }},

		{ { -0.25f,  0.0f, 0.0f } ,{ 1.0f, 1.0f, 1.0f, 1.0f }},
		{ { 0.25f,   0.0f, 0.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f }},
		{ { 0.25f,  -0.5f, 0.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f }},

		{ { -0.35f,  0.0f, 0.0f } ,{ 1.0f, 1.0f, 1.0f, 1.0f }},
		{ { 0.0f,    0.5f, 0.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f }},
		{ { 0.35f,   0.0f, 0.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f }},

		{ { 0.062f,  0.0f, 0.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f }},
		{ { 0.22f,   0.5f, 0.0f } ,{ 1.0f, 1.0f, 1.0f, 1.0f }},
		{ { 0.22f,   0.0f, 0.0f } ,{ 1.0f, 1.0f, 1.0f, 0.0f }},

		{ { 0.062f,   0.5f, 0.0f } ,{ 1.0f, 1.0f, 1.0f, 1.0f }},
		{ { 0.22f,    0.5f, 0.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f }},
		{ { 0.062f,   0.0f, 0.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f }},

		{ { -0.6f,   0.7f, 0.0f } ,{ 1.0f, 1.0f, 1.0f, 1.0f }},
		{ { -0.4f,   0.7f, 0.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f }},
		{ { -0.5f,   0.45f, 0.0f } ,{ 1.0f, 1.0f, 1.0f, 1.0f }},

		{ { -0.5f,   0.8f, 0.0f } ,{ 1.0f, 1.0f, 1.0f, 1.0f }},
		{ { -0.4f,   0.55f, 0.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f }},
		{ { -0.6f,   0.55f, 0.0f } ,{ 1.0f, 1.0f, 1.0f, 0.0f }},


		//{ { -0.25f, 0.0f, 0.0f } ,{ 1.0f, 1.0f, 1.0f, 1.0f } },
		//{ { 0.0f,  0.5f, 0.0f }  ,{ 1.0f, 1.0f, 0.0f, 1.0f } },
		//{ { 0.25f, 0.0f, 0.0f } ,{ 1.0f, 1.0f, 1.0f, 0.0f } }

		//{ { -0.5f, -0.5f, 0.0f } },
		//{ { -0.25f, 0.0f, 0.0f } },
		//{ { 0.0f,  -0.5f, 0.0f } },

		//{ { -0.25f, 0.0f, 0.0f } },
		//{ { 0.0f,   0.5f, 0.0f } },
		//{ { 0.25f,  0.0f, 0.0f } },

		//{ { 0.0f, -0.5f, 0.0f } },
		//{ { 0.25f, 0.0f, 0.0f } },
		//{ { 0.5f, -0.5f, 0.0f } }
	};
	
	const uint32_t sIndices[] =
	{
		0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
	};
}

GameApp::GameApp()
{
}

GameApp::~GameApp()
{
}

void GameApp::OnInitialize()
{
	mWindow.Initialize(GetInstance(), GetAppName(), 1280, 720);
	Graphics::GraphicsSystem::StaticInitialize(mWindow.GetWindowHandle(), false);

	mMeshBuffer.Initialize(sVertices, sizeof(Graphics::VertexPC), std::size(sVertices), std::size(sIndices), sIndices);

	mVertexShader.Initialize(L"../Assets/Shaders/DoSomething.fx", Graphics::VertexPC::Format);
	mPixelShader.Initialize(L"../Assets/Shaders/DoSomething.fx");
}

void GameApp::OnTerminate()
{
	mVertexShader.Terminate();
	mPixelShader.Terminate();
	mMeshBuffer.Terminate();

	Graphics::GraphicsSystem::StaticTerminate();
	mWindow.Terminate();
}

void GameApp::OnUpdate()
{
	if (mWindow.ProcessMessage())
	{
		Kill();
		return;
	}

	if (GetAsyncKeyState(VK_ESCAPE))
	{
		PostQuitMessage(0);
		return;
	}

	Graphics::GraphicsSystem::Get()->BeginRender();

	mVertexShader.Bind();
	mPixelShader.Bind();
	mMeshBuffer.Draw(sizeof(Graphics::VertexPC), std::size(sIndices));

	Graphics::GraphicsSystem::Get()->EndRender();
}
