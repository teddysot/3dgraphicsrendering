#ifndef INCLUDED_GAMEAPP_H
#define INCLUDED_GAMEAPP_H

#include <Core/Inc/Core.h>
#include <Graphics/Inc/Graphics.h>
#include <Input/Inc/Input.h>
#include <Math/Inc/EngineMath.h>

class GameApp : public Engine::Core::Application
{
public:
	GameApp();
	~GameApp() override;

private:
	void OnInitialize() override;
	void OnTerminate() override;
	void OnUpdate() override;


	struct ConstantData
	{
		Engine::Math::Matrix wvp;
		Engine::Math::Matrix World;
		Engine::Math::Vector3 viewPosition;
		float padding;
		Engine::Graphics::DirectionalLight light;
		Engine::Graphics::Material material;
	};
	struct ConstantData1
	{
		Engine::Math::Matrix wvp1;
		Engine::Math::Matrix World1;
		Engine::Math::Vector3 viewPosition1;
		float padding1;
		Engine::Graphics::DirectionalLight light1;
		Engine::Graphics::Material material1;
	};

	struct ConstantData2
	{
		Engine::Math::Matrix wvp;
		Engine::Math::Matrix World;
		Engine::Math::Vector3 viewPosition;
		float padding;
		Engine::Graphics::DirectionalLight light;
		Engine::Graphics::Material material;
	};





	Engine::Core::Window mWindow;
	Engine::Input::InputSystem mInput;
	Engine::Graphics::Camera mCamera;
	Engine::Graphics::ConstantBuffer mConstantBuffer;

	Engine::Graphics::MeshS mSkyDome;
	Engine::Graphics::MeshBuilder mSkyDomeBuilder;
	Engine::Graphics::MeshBuffer mSkyDomeBuffer;
	Engine::Graphics::VertexShader mSkyDomeVertexShader;
	Engine::Graphics::PixelShader mSkyDomePixelShader;
	Engine::Graphics::Texture mSkyDomeTexture;

	Engine::Graphics::MeshS mSphere;
	Engine::Graphics::MeshBuilder mSphereBuilder;
	Engine::Graphics::MeshBuffer mSphereBuffer;
	Engine::Graphics::VertexShader mSphereVertexShader;
	Engine::Graphics::PixelShader mSpherePixelShader;
	Engine::Graphics::Texture mSphereTexture;

	Engine::Graphics::MeshS mSun;
	Engine::Graphics::MeshBuilder mSunBuilder;
	Engine::Graphics::MeshBuffer mSunBuffer;
	Engine::Graphics::Texture mSunTexture;

	Engine::Graphics::MeshS mBall;
	Engine::Graphics::MeshBuilder mBallBuilder;
	Engine::Graphics::MeshBuffer mBallBuffer;
	Engine::Graphics::Texture mBallTexture;


	Engine::Graphics::TypedConstantBuffer<ConstantData> mConstantBufferS;
	Engine::Graphics::TypedConstantBuffer<ConstantData1> mConstantBufferB;
	Engine::Graphics::TypedConstantBuffer<ConstantData2> mConstantBufferC;

	Engine::Graphics::DirectionalLight mLight;
	Engine::Graphics::Material mMaterial;
	Engine::Graphics::VertexShader mLightVertexShader;
	Engine::Graphics::PixelShader mLightPixelShader;

	Engine::Graphics::Texture Diffuse;
	Engine::Graphics::Texture Specular;
	Engine::Graphics::Texture DarkSide;
	Engine::Graphics::Texture Normal;
	Engine::Graphics::Texture Bumpmap;

	Engine::Graphics::Texture Diffuse2;
	Engine::Graphics::Texture Specular2;
	Engine::Graphics::Texture DarkSide2;
	Engine::Graphics::Texture Normal2;
	Engine::Graphics::Texture Bumpmap2;

	Engine::Graphics::Texture Diffuse3;
	Engine::Graphics::Texture Specular3;
	Engine::Graphics::Texture DarkSide3;
	Engine::Graphics::Texture Normal3;
	Engine::Graphics::Texture Bumpmap3;


	// for planet rotation
	float rotationY = 0.0f;
	float rotationX = 0.0f;

	// switch for rotation and orbiting
	bool Switch = false;
	bool Switch1 = false;
	bool Switch2 = false;
	bool Switch3 = false;

	// field of view
	float FOV = 1.0f;

	// for planet orbiting speed
	float alpha1 = 0;
	float alpha2 = 0;
	float alpha3 = 0;
	float alpha4 = 0;
	float alpha5 = 0;
	float alpha6 = 0;
	float alpha7 = 0;
	float alpha8 = 0;
	float alpha9 = 0;
};

#endif //#ifndef INCLUDED_GAMEAPP_H