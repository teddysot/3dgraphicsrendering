#include "GameApp.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	GameApp myApp;
	myApp.Initialize(hInstance, "Hello Cube!");

	while (myApp.IsRunning())
	{
		myApp.Update();
	}

	myApp.Terminate();
	return 0;
}

/*
// Interface --> a list of public pure virtual function declarations

class IGameSystem
{
	virtual void
};

class GameSystem
{
public:
	void DoA();
	void DoB();
	void DoC();
}

device context -> make draw call, render command
device -> memory management
*/