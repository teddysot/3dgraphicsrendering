#include "GameApp.h"
#include <windows.h>
using namespace Engine;

/*
For Homework
Finish the implementation for this class
	- Add Getters and setters for the members
	- Plus anuthing you think will be convinent
	- Add a new project called HelloCamera
	- Setup a field of cubes so you have a frame of reference
	- Use the camera class to get the view/proj matrices
	- Use Input to control camera (WASD and yaw/pitch)
*/

namespace //Global stuff only used in this cpp file
{
	// Define our triangle vertices
	const Graphics::VertexPC sVertices[] =
	{
		{ { -1.0f, -1.0, -1.0f } ,{ 1.0f, 0.0f, 0.0f, 0.0f } }, //FRONT
		{ { -1.0f,  1.0f, -1.0f } ,{ 1.0f, 0.0f, 0.0f, 0.0f } },
		{ { 1.0f,  -1.0f, -1.0f } ,{ 1.0f, 0.0f, 0.0f, 0.0f } },

		{ { -1.0f, 1.0, -1.0f } ,{ 1.0f, 0.0f, 0.0f, 0.0f } }, //FRONT
		{ { 1.0f,  1.0f, -1.0f } ,{ 1.0f, 0.0f, 0.0f, 0.0f } },
		{ { 1.0f,  -1.0f, -1.0f } ,{ 1.0f, 0.0f, 0.0f, 0.0f } },




		{ { -1.0f, -1.0, 1.0f } ,{ 0.0f, 1.0f, 0.0f, 0.0f } }, //BACK
		{ { 1.0f,  -1.0f, 1.0f } ,{ 0.0f, 1.0f, 0.0f, 0.0f } },
		{ { -1.0f,  1.0f, 1.0f } ,{ 0.0f, 1.0f, 0.0f, 0.0f } },

		{ { 1.0f,  1.0f, 1.0f } ,{ 0.0f, 1.0f, 0.0f, 0.0f } }, //BACK
		{ { -1.0f, 1.0, 1.0f } ,{ 0.0f, 1.0f, 0.0f, 0.0f } },
		{ { 1.0f,  -1.0f, 1.0f } ,{ 0.0f, 1.0f, 0.0f, 0.0f } },




		{ { -1.0f,  1.0f, -1.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f } },  //LEFT
		{ { -1.0f,  -1.0f, -1.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f } },
		{ { -1.0f,  -1.0f, 1.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f } },

		{ { -1.0f,  1.0f, 1.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f } }, //LEFT
		{ { -1.0f,  1.0f, -1.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f } },
		{ { -1.0f,  -1.0f, 1.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f } },




		{ { 1.0f,  -1.0f, -1.0f } ,{ 0.0f, 0.0f, 1.0f, 1.0f } }, //RIGHT
		{ { 1.0f,  1.0f, -1.0f } ,{ 0.0f, 0.0f, 1.0f, 1.0f } },
		{ { 1.0f,  -1.0f, 1.0f } ,{ 0.0f, 0.0f, 1.0f, 1.0f } },

		{ { 1.0f,  1.0f, -1.0f } ,{ 0.0f, 0.0f, 1.0f, 1.0f } }, //RIGHT
		{ { 1.0f,  1.0f, 1.0f } ,{ 0.0f, 0.0f, 1.0f, 1.0f } },
		{ { 1.0f,  -1.0f, 1.0f } ,{ 0.0f, 0.0f, 1.0f, 1.0f } },




		{ { 1.0f,  1.0f, -1.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f } }, //TOP
		{ { -1.0f,  1.0f, -1.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f } },
		{ { 1.0f,  1.0f, 1.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f } },

		{ { -1.0f,  1.0f, -1.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f } }, //TOP
		{ { -1.0f,  1.0f, 1.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f } },
		{ { 1.0f,  1.0f, 1.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f } },




		{ { 1.0f,  -1.0f, -1.0f } ,{ 1.0f, 0.5f, 0.0f, 1.0f } }, //BOTTOM
		{ { 1.0f,  -1.0f, 1.0f } ,{ 1.0f, 0.5f, 0.0f, 1.0f } },
		{ { -1.0f,  -1.0f, -1.0f } ,{ 1.0f, 0.5f, 0.0f, 1.0f } },

		{ { -1.0f,  -1.0f, 1.0f } ,{ 1.0f, 0.5f, 0.0f, 1.0f } }, //BOTTOM
		{ { -1.0f,  -1.0f, -1.0f } ,{ 1.0f, 0.5f, 0.0f, 1.0f } },
		{ { 1.0f,  -1.0f, 1.0f } ,{ 1.0f, 0.5f, 0.0f, 1.0f } },

	};

	const uint32_t sIndices[] =
	{
		0,1,2,
		3,4,5,

		6,7,8,
		9,10,11,

		12,13,14,
		15,16,17,

		18,19,20,
		21,22,23,

		24,25,26,
		27,28,29,

		30,31,32,
		33,34,35
	};
}

GameApp::GameApp()
	: mConstantBuffer{ nullptr }
{
}

GameApp::~GameApp()
{
}

void GameApp::OnInitialize()
{
	mWindow.Initialize(GetInstance(), GetAppName(), 1270, 720);
	Graphics::GraphicsSystem::StaticInitialize(mWindow.GetWindowHandle(), false);

	Input::InputSystem::StaticInitialize(mWindow.GetWindowHandle());
	mMeshBuffer.Initialize(sVertices, sizeof(Graphics::VertexPC), std::size(sVertices), std::size(sIndices), sIndices);

	mVertexShader.Initialize(L"../Assets/Shaders/DoTransform.fx", Graphics::VertexPC::Format);
	mPixelShader.Initialize(L"../Assets/Shaders/DoTransform.fx");
	//mConstantBuffer.Initialize();


	mCamera.SetPosition(Math::Vector3(0.0f, 10.0f, -25.0f));
	mCamera.SetDirection(Math::Vector3(0.0f, 0.0f, 1.0f));

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = sizeof(Math::Matrix) * 3;
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	Graphics::GraphicsSystem::Get()->GetDevice()->CreateBuffer(&bufferDesc, nullptr, &mConstantBuffer);

}

void GameApp::OnTerminate()
{
	mVertexShader.Terminate();
	mPixelShader.Terminate();
	mMeshBuffer.Terminate();
	SafeRelease(mConstantBuffer);

	//mConstantBuffer.Terminate();

	Graphics::GraphicsSystem::StaticTerminate();
	mWindow.Terminate();
}

void GameApp::OnUpdate()
{
	if (mWindow.ProcessMessage())
	{
		Kill();
		return;
	}

	if (GetAsyncKeyState(VK_ESCAPE))
	{
		PostQuitMessage(0);
		return;
	}

	Graphics::GraphicsSystem::Get()->BeginRender();

	mVertexShader.Bind();
	mPixelShader.Bind();

	float cameraMoveSpeed = 3.0f;
	float cameraRotationSpeed = 0.05f;
	float cameraSpeedValue = 2.0f;
	float rise = 1.0f;
	float speed = 0.0f;

	if (GetAsyncKeyState(VK_SPACE)) // speed increase
	{
		speed = 5;
	}

	if (GetAsyncKeyState(VK_LEFT))
	{
		rotationY = rotationY + 0.1;
	}

	else if (GetAsyncKeyState(VK_RIGHT))
	{
		rotationY = rotationY - 0.1;
	}

	if (GetAsyncKeyState(VK_UP))
	{
		rotationX = rotationX + 0.1;
	}

	else if (GetAsyncKeyState(VK_DOWN))
	{
		rotationX = rotationX - 0.1;
	}

	if (GetAsyncKeyState('P') & 1) // play
	{
		Switch = !Switch;
	}

	if (Switch == true)
	{
		rotationY = rotationY + 0.1;
	}

	Input::InputSystem* is = Input::InputSystem::Get();
	is->Update();

	if (GetAsyncKeyState(VK_RBUTTON)) // right click to move
	{
		mCamera.Pitch((is->GetMouseMoveY() * cameraRotationSpeed * 0.05f)); // up down
		mCamera.Yaw((is->GetMouseMoveX() * cameraRotationSpeed * 0.05)); // left right
	}

	Graphics::GraphicsSystem* gs = Graphics::GraphicsSystem::Get();
	Math::Matrix viewMatrix = mCamera.GetViewMatrix();
	Math::Matrix projectionMatrix = mCamera.GetPerspectiveMatrix(gs->GetAspectRatio());


	mCamera.SetFOV(FOV);

	if (GetAsyncKeyState('M')) // zoom out out to the furtest view
	{
		if (FOV <= 2.5f)
		{
			FOV = FOV + 0.05f;
		}

	}

	if (GetAsyncKeyState('N')) // zoom in to the nearest view
	{
		if (FOV >= 0.0f)
		{
			FOV = FOV - 0.05f;
		}
	}

	if (GetAsyncKeyState('B')) // reset the view to 1
	{

		FOV = 1.0f;
	}




	Math::Vector3 movement;

	if (GetAsyncKeyState('W'))
	{
		movement.z = cameraMoveSpeed + speed;
	}
	if (GetAsyncKeyState('S'))
	{
		movement.z = -cameraMoveSpeed - speed;
	}

	if (GetAsyncKeyState('D'))
	{
		movement.x = cameraMoveSpeed + speed;
	}
	
	if (GetAsyncKeyState('A'))
	{
		movement.x = -cameraMoveSpeed - speed;
	}

	if (GetAsyncKeyState('E'))
	{
		mCamera.Rise(cameraMoveSpeed * 0.1f);
	}

	if (GetAsyncKeyState('Q'))
	{
		mCamera.Fall(cameraMoveSpeed * 0.1f);
	}

	mCamera.Walk(movement.z * 0.1f); // forward backward
	mCamera.Strafe(movement.x * 0.1f); // left right

	auto context = Graphics::GraphicsSystem::Get()->GetContext();
	


	

/*-----------------------------------------------------------------------------------------------------------------------------------*/ // Draw a field of cubes
	Math::Matrix TRANSLATION;
	Math::Matrix WORLD;
	Math::Matrix Matrices[3];

	for (int i = -200; i < 204; i = i + 4)
	{
		for (int j = -200; j < 204; j = j + 4)
		{
			TRANSLATION = Math::Matrix::Translation(i, 0, j);
			WORLD = Math::Matrix::RotationY(rotationY)*Math::Matrix::RotationX(rotationX)*TRANSLATION;

			Matrices[0] = Math::Transpose(WORLD);
			Matrices[1] = Math::Transpose(viewMatrix);
			Matrices[2] = Math::Transpose(projectionMatrix);
			context->UpdateSubresource(mConstantBuffer, 0, nullptr, Matrices, 0, 0);
			context->VSSetConstantBuffers(0, 1, &mConstantBuffer);
			mMeshBuffer.Draw(sizeof(Graphics::VertexPC), std::size(sIndices));
		}
	}
/*-----------------------------------------------------------------------------------------------------------------------------------*/ // Draw a field of cubes

	if (GetAsyncKeyState('L') & 1)
	{
		mCamera.SetLookAt(Math::Vector3(1.0f, 0.0f, 0.0f));
	}


	




	Graphics::GraphicsSystem::Get()->EndRender();
}


//