#ifndef INCLUDED_GAMEAPP_H
#define INCLUDED_GAMEAPP_H

#include <Core/Inc/Core.h>
#include <Graphics/Inc/Graphics.h>
#include <Input/Inc/Input.h>
#include <Math/Inc/EngineMath.h>

class GameApp : public Engine::Core::Application
{
public:
	GameApp();
	~GameApp() override;

private:
	void OnInitialize() override;
	void OnTerminate() override;
	void OnUpdate() override;

	Engine::Core::Window mWindow;

	Engine::Graphics::VertexShader mVertexShader;
	Engine::Graphics::PixelShader mPixelShader;
	Engine::Graphics::MeshBuffer mMeshBuffer;
	//Engine::Graphics::ConstantBuffer mConstantBuffer;

	Engine::Graphics::Camera mCamera;
	Engine::Input::InputSystem mInput;

	float rotationY = 0.0f;
	float rotationX = 0.0f;

	bool Switch = false;

	float FOV = 1.0f;

	ID3D11Buffer* mConstantBuffer;
};

#endif //#ifndef INCLUDED_GAMEAPP_H