#include "GameApp.h"
#include <windows.h>
using namespace Engine;

/*
For Homework
Finish the implementation for this class
- Add Getters and setters for the members
- Plus anuthing you think will be convinent
- Add a new project called HelloCamera
- Setup a field of cubes so you have a frame of reference
- Use the camera class to get the view/proj matrices
- Use Input to control camera (WASD and yaw/pitch)
*/

namespace //Global stuff only used in this cpp file
{

}

GameApp::GameApp()
{
}

GameApp::~GameApp()
{
}

void GameApp::OnInitialize()
{
	mWindow.Initialize(GetInstance(), GetAppName(), 1270, 720);
	Graphics::GraphicsSystem::StaticInitialize(mWindow.GetWindowHandle(), false);

	Input::InputSystem::StaticInitialize(mWindow.GetWindowHandle());
	mCamera.SetPosition(Math::Vector3(0.0f, 0.0f, -6.0f));
	mCamera.SetDirection(Math::Vector3(0.0f, 0.0f, 1.0f));


	//mVertexShader.Initialize(L"../Assets/Shaders/DoTransform.fx", Graphics::VertexPC::Format);
	//mPixelShader.Initialize(L"../Assets/Shaders/DoTransform.fx");

	mConstantBuffer.Initialize();

	mVertexShader.Initialize(L"../Assets/Shaders/Texturing.fx", Graphics::VertexPT::Format);
	mPixelShader.Initialize(L"../Assets/Shaders/Texturing.fx");

	mTexture.Initialize("../Assets/Images/qmark.jpg");
	mSampler.Initialize(Graphics::Sampler::Filter::Linear, Graphics::Sampler::AddressMode::Clamp);



	mVertexShaderSphere.Initialize(L"../Assets/Shaders/Texturing.fx", Graphics::VertexS::Format);
	mPixelShaderSphere.Initialize(L"../Assets/Shaders/Texturing.fx");

	mTextureSphere.Initialize("../Assets/Images/soccer.jpg");
	mSamplerSphere.Initialize(Graphics::Sampler::Filter::Linear, Graphics::Sampler::AddressMode::Clamp);

	mCubeBuilder.GenerateCubePT(mCubeTwo);
	mMeshBuffer.Initialize(mCubeTwo);

	mSphereBuilder.GenerateSphereS(mSphere, 50, 50);
	mSphereBuffer.Initialize(mSphere);

	mPyramidBuilder.GeneratePyramidPT(Pyramid);
	mPyramidBuffer.Initialize(Pyramid);
	mPyramidTexture.Initialize("../Assets/Images/galactic2.jpg");


	//mCubeBuilder.GenerateCubePC(mCube);
	//mMeshBuffer.Initialize(mCube);

	//mSamplers.Initialize(Graphics::Sampler::Filter::Point, Graphics::Sampler::AddressMode::Clamp);
	//mMeshBuffer.Initialize(kVertices, sizeof(Graphics::VertexPT), kVertexCount, kIndexCount, kIndices);
}

void GameApp::OnTerminate()
{
	mVertexShader.Terminate();
	mPixelShader.Terminate();
	mMeshBuffer.Terminate();
	mSphereBuffer.Terminate();
	mTexture.Terminate();
	mSampler.Terminate();

	mVertexShaderSphere.Terminate();
	mPixelShaderSphere.Terminate();
	mTextureSphere.Terminate();
	mSamplerSphere.Terminate();

	mConstantBuffer.Terminate();

	Pyramid.Destroy();
	mPyramidBuffer.Terminate();
	mPyramidTexture.Terminate();

	Graphics::GraphicsSystem::StaticTerminate();
	mWindow.Terminate();
}

void GameApp::OnUpdate()
{
	if (mWindow.ProcessMessage())
	{
		Kill();
		return;
	}

	if (GetAsyncKeyState(VK_ESCAPE))
	{
		PostQuitMessage(0);
		return;
	}

	Graphics::GraphicsSystem::Get()->BeginRender();

	

	float cameraMoveSpeed = 3.0f;
	float cameraRotationSpeed = 0.05f;
	float cameraSpeedValue = 2.0f;
	float rise = 1.0f;
	float speed = 0.0f;

	if (GetAsyncKeyState(VK_SPACE)) // speed increase
	{
		speed = 5;
	}

	if (GetAsyncKeyState(VK_LEFT))
	{
		rotationY = rotationY + 0.1;
	}

	else if (GetAsyncKeyState(VK_RIGHT))
	{
		rotationY = rotationY - 0.1;
	}

	if (GetAsyncKeyState(VK_UP))
	{
		rotationX = rotationX + 0.1;
	}

	else if (GetAsyncKeyState(VK_DOWN))
	{
		rotationX = rotationX - 0.1;
	}

	if (GetAsyncKeyState('P') & 1) // play
	{
		Switch = !Switch;
	}

	if (Switch == true)
	{
		rotationY = rotationY + 0.1;
	}

	Input::InputSystem* is = Input::InputSystem::Get();
	is->Update();

	if (GetAsyncKeyState(VK_RBUTTON)) // right click to move
	{
		mCamera.Pitch((is->GetMouseMoveY() * cameraRotationSpeed * 0.05f)); // up down
		mCamera.Yaw((is->GetMouseMoveX() * cameraRotationSpeed * 0.05)); // left right
	}

	Graphics::GraphicsSystem* gs = Graphics::GraphicsSystem::Get();

	Math::Matrix viewMatrix = mCamera.GetViewMatrix();
	Math::Matrix projectionMatrix = mCamera.GetPerspectiveMatrix(gs->GetAspectRatio());

	mCamera.SetFOV(FOV);

	if (GetAsyncKeyState('M')) // zoom out out to the furtest view
	{
		if (FOV <= 2.5f)
		{
			FOV = FOV + 0.05f;
		}

	}

	if (GetAsyncKeyState('N')) // zoom in to the nearest view
	{
		if (FOV >= 0.0f)
		{
			FOV = FOV - 0.05f;
		}
	}

	if (GetAsyncKeyState('B')) // reset the view to 1
	{

		FOV = 1.0f;
	}

	Math::Vector3 movement;

	if (GetAsyncKeyState('W'))
	{
		movement.z = cameraMoveSpeed + speed;
	}
	
	if (GetAsyncKeyState('S'))
	{
		movement.z = -cameraMoveSpeed - speed;
	}

	if (GetAsyncKeyState('D'))
	{
		movement.x = cameraMoveSpeed + speed;
	}

	if (GetAsyncKeyState('A'))
	{
		movement.x = -cameraMoveSpeed - speed;
	}

	if (GetAsyncKeyState('E'))
	{
		mCamera.Rise(cameraMoveSpeed * 0.1f);
	}

	if (GetAsyncKeyState('Q'))
	{
		mCamera.Fall(cameraMoveSpeed * 0.1f);
	}
	
	if (GetAsyncKeyState('L') & 1)
	{
		mCamera.SetLookAt(Math::Vector3(1.0f, 0.0f, 0.0f));
	}


	mCamera.Walk(movement.z * 0.1f); // forward backward
	mCamera.Strafe(movement.x * 0.1f); // left right

	auto context = Graphics::GraphicsSystem::Get()->GetContext();
	
	mVertexShader.Bind();
	mPixelShader.Bind();

	mTexture.BindPS(0);
	mSampler.BindPS(0);

	/*-----------------------------------------------------------------------------------------------------------------------------------*/
	Math::Matrix TRANSLATION;
	Math::Matrix WORLD;
	Math::Matrix Matrices[3];

	TRANSLATION = Math::Matrix::Translation(-3, 0, 0);
	WORLD = Math::Matrix::RotationY(rotationY)*Math::Matrix::RotationX(rotationX)*TRANSLATION;

	Matrices[0] = Math::Transpose(WORLD*viewMatrix*projectionMatrix);
	//Matrices[1] = Math::Transpose(viewMatrix);
	//Matrices[2] = Math::Transpose(projectionMatrix);
			
	mConstantBuffer.Set(Matrices);
	mConstantBuffer.BindVS();

	mMeshBuffer.Draw();

	/*-----------------------------------------------------------------------------------------------------------------------------------*/
	
	
	mVertexShaderSphere.Bind();
	mPixelShaderSphere.Bind();

	mTextureSphere.BindPS(0);
	mSamplerSphere.BindPS(0);


	Math::Matrix TRANSLATION1;
	Math::Matrix WORLD1;
	Math::Matrix Matrices1[3];

	TRANSLATION1 = Math::Matrix::Translation(3, 0, 0);
	WORLD1 = Math::Matrix::RotationY(rotationY)*Math::Matrix::RotationX(rotationX)*TRANSLATION1;

	Matrices1[0] = Math::Transpose(WORLD1*viewMatrix*projectionMatrix);


	mConstantBuffer.Set(Matrices1);
	mConstantBuffer.BindVS();

	mSphereBuffer.Draw();

	/*-----------------------------------------------------------------------------------------------------------------------------------*/

	mVertexShader.Bind();
	mPixelShader.Bind();
	mPyramidTexture.BindPS(0);

	Math::Matrix TRANSLATION_P;
	Math::Matrix WORLD_P;
	Math::Matrix Matrices_P[3];

	TRANSLATION_P = Math::Matrix::Translation(0, 0, 0);
	WORLD_P = Math::Matrix::RotationY(rotationY)*Math::Matrix::RotationX(rotationX)*TRANSLATION_P;

	Matrices_P[0] = Math::Transpose(WORLD_P);
	Matrices_P[1] = Math::Transpose(viewMatrix);
	Matrices_P[2] = Math::Transpose(projectionMatrix);

	mConstantBuffer.Set(Matrices_P);
	mConstantBuffer.BindVS();

	mPyramidBuffer.Draw();

	/*-----------------------------------------------------------------------------------------------------------------------------------*/

	Graphics::GraphicsSystem::Get()->EndRender();
}


//