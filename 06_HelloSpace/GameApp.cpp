#include "GameApp.h"
#include <windows.h>
using namespace Engine;

namespace //Global stuff only used in this cpp file
{

}

GameApp::GameApp()
{
}

GameApp::~GameApp()
{
}

void GameApp::OnInitialize()
{
	mWindow.Initialize(GetInstance(), GetAppName(), 1270, 720);
	Graphics::GraphicsSystem::StaticInitialize(mWindow.GetWindowHandle(), false);

	Input::InputSystem::StaticInitialize(mWindow.GetWindowHandle());

	mConstantBuffer.Initialize();
	mSphereVertexShader.Initialize(L"../Assets/Shaders/Texturing.fx", Graphics::VertexS::Format);
	mSpherePixelShader.Initialize(L"../Assets/Shaders/Texturing.fx");
	mCamera.SetPosition(Math::Vector3(0.0f, 20.0f, -300.0f));
	mCamera.SetDirection(Math::Vector3(0.0f, 0.0f, 1.0f));

	// Skydome
	/*----------------------------------------------------------------------------------------------------------------*/
	mSkyDomeBuilder.GenerateSkydomeS(mSkyDome, 50, 50);
	mSkyDomeBuffer.Initialize(mSkyDome);
	mSkyDomeTexture.Initialize("../Assets/Images/8k_stars_milky_way.jpg");
	/*----------------------------------------------------------------------------------------------------------------*/

	// Sun
	/*----------------------------------------------------------------------------------------------------------------*/
	mSunBuilder.GenerateSphereS(mSun, 50, 50);
	mSunBuffer.Initialize(mSun);
	mSunTexture.Initialize("../Assets/Images/8k_sun.jpg");
	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 1
	/*----------------------------------------------------------------------------------------------------------------*/
	mMercuryBuilder.GenerateSphereS(mMercury, 50, 50);
	mMercuryBuffer.Initialize(mMercury);
	mMercuryTexture.Initialize("../Assets/Images/2k_mercury.jpg");
	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 2
	/*----------------------------------------------------------------------------------------------------------------*/
	mVenusBuilder.GenerateSphereS(mVenus, 50, 50);
	mVenusBuffer.Initialize(mVenus);
	mVenusTexture.Initialize("../Assets/Images/2k_venus_atmosphere.jpg");
	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 3
	/*----------------------------------------------------------------------------------------------------------------*/
	mEarthBuilder.GenerateSphereS(mEarth, 50, 50);
	mEarthBuffer.Initialize(mEarth);
	mEarthTexture.Initialize("../Assets/Images/earth1.jpg");
	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 4
	/*----------------------------------------------------------------------------------------------------------------*/
	mMarsBuilder.GenerateSphereS(mMars, 50, 50);
	mMarsBuffer.Initialize(mMars);
	mMarsTexture.Initialize("../Assets/Images/2k_mars.jpg");
	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 5
	/*----------------------------------------------------------------------------------------------------------------*/
	mJupiterBuilder.GenerateSphereS(mJupiter, 50, 50);
	mJupiterBuffer.Initialize(mJupiter);
	mJupiterTexture.Initialize("../Assets/Images/2k_jupiter.jpg");
	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 6
	/*----------------------------------------------------------------------------------------------------------------*/
	mSaturnBuilder.GenerateSphereS(mSaturn, 50, 50);
	mSaturnBuffer.Initialize(mSaturn);
	mSaturnTexture.Initialize("../Assets/Images/2k_saturn.jpg");
	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 7
	/*----------------------------------------------------------------------------------------------------------------*/
	mUranusBuilder.GenerateSphereS(mUranus, 50, 50);
	mUranusBuffer.Initialize(mUranus);
	mUranusTexture.Initialize("../Assets/Images/2k_uranus.jpg");
	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 8
	/*----------------------------------------------------------------------------------------------------------------*/
	mNeptuneBuilder.GenerateSphereS(mNeptune, 50, 50);
	mNeptuneBuffer.Initialize(mNeptune);
	mNeptuneTexture.Initialize("../Assets/Images/2k_neptune.jpg");
	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 9
	/*----------------------------------------------------------------------------------------------------------------*/
	mPlutoBuilder.GenerateSphereS(mPluto, 50, 50);
	mPlutoBuffer.Initialize(mPluto);
	mPlutoTexture.Initialize("../Assets/Images/pluto.jpg");
	/*----------------------------------------------------------------------------------------------------------------*/
}

void GameApp::OnTerminate()
{
	mWindow.Terminate();
	Graphics::GraphicsSystem::StaticTerminate();
	mConstantBuffer.Terminate();
	
	mSpherePixelShader.Terminate();
	mSphereVertexShader.Terminate();

	mSkyDome.Destroy();
	mSkyDomeTexture.Terminate();
	mSkyDomeBuffer.Terminate();

	mSun.Destroy();
	mSunTexture.Terminate();
	mSunBuffer.Terminate();

	mMercury.Destroy();
	mMercuryTexture.Terminate();
	mMercuryBuffer.Terminate();

	mVenus.Destroy();
	mVenusTexture.Terminate();
	mVenusBuffer.Terminate();

	mEarth.Destroy();
	mEarthTexture.Terminate();
	mEarthBuffer.Terminate();

	mMars.Destroy();
	mMarsTexture.Terminate();
	mMarsBuffer.Terminate();

	mJupiter.Destroy();
	mJupiterTexture.Terminate();
	mJupiterBuffer.Terminate();

	mSaturn.Destroy();
	mSaturnTexture.Terminate();
	mSaturnBuffer.Terminate();

	mUranus.Destroy();
	mUranusTexture.Terminate();
	mUranusBuffer.Terminate();

	mNeptune.Destroy();
	mNeptuneTexture.Terminate();
	mNeptuneBuffer.Terminate();

	mPluto.Destroy();
	mPlutoTexture.Terminate();
	mPlutoBuffer.Terminate();
}

void GameApp::OnUpdate()
{
	if (mWindow.ProcessMessage())
	{
		Kill();
		return;
	}

	if (GetAsyncKeyState(VK_ESCAPE))
	{
		PostQuitMessage(0);
		return;
	}

	Graphics::GraphicsSystem::Get()->BeginRender();
	
	float speed = 0.0f;
	float cameraMoveSpeed = 3.0f;
	float cameraRotationSpeed = 0.05f;
	float cameraSpeedValue = 2.0f;

	if (GetAsyncKeyState(VK_SPACE)) // speed increase
	{
		speed = 40;
		cameraMoveSpeed = 20;
	}

	if (GetAsyncKeyState(VK_LEFT))
	{
		rotationY = rotationY + 0.1;
	}

	else if (GetAsyncKeyState(VK_RIGHT))
	{
		rotationY = rotationY - 0.1;
	}

	if (GetAsyncKeyState(VK_UP))
	{
		rotationX = rotationX + 0.1;
	}

	else if (GetAsyncKeyState(VK_DOWN))
	{
		rotationX = rotationX - 0.1;
	}

	if (GetAsyncKeyState('P') & 1) // play
	{
		Switch = !Switch;
	}

	if (Switch == true)
	{
		rotationY = rotationY - 0.01;
	}

	if (GetAsyncKeyState('O') & 1) // play
	{
		Switch1 = !Switch1;
	}

	if (Switch1 == true)
	{
		alpha1 = alpha1 + 1;
		alpha2 = alpha2 + 0.9;
		alpha3 = alpha3 + 0.86;
		alpha4 = alpha4 + 0.84;
		alpha5 = alpha5 + 0.77;
		alpha6 = alpha6 + 0.72;
		alpha7 = alpha7 + 0.68;
		alpha8 = alpha8 + 0.63;
		alpha9 = alpha9 + 0.59;
	}

	if (GetAsyncKeyState('L') & 1) // play
	{
		Switch2 = !Switch2;
	}

	if (Switch2 == true)
	{
		mCamera.SetLookAt(Math::Vector3(1.0f, 0.0f, 0.0f));
	}

	Input::InputSystem* is = Input::InputSystem::Get();
	is->Update();

	if (GetAsyncKeyState(VK_RBUTTON)) // right click to move
	{
		mCamera.Pitch((is->GetMouseMoveY() * cameraRotationSpeed * 0.05f)); // up down
		mCamera.Yaw((is->GetMouseMoveX() * cameraRotationSpeed * 0.05)); // left right
	}

	Graphics::GraphicsSystem* gs = Graphics::GraphicsSystem::Get();

	Math::Matrix viewMatrix = mCamera.GetViewMatrix();
	Math::Matrix projectionMatrix = mCamera.GetPerspectiveMatrix(gs->GetAspectRatio());

	mCamera.SetFOV(FOV);

	if (GetAsyncKeyState('M')) // zoom out out to the furtest view
	{
		if (FOV <= 2.5f)
		{
			FOV = FOV + 0.05f;
		}

	}

	if (GetAsyncKeyState('N')) // zoom in to the nearest view
	{
		if (FOV >= 0.0f)
		{
			FOV = FOV - 0.05f;
		}
	}

	if (GetAsyncKeyState('B')) // reset the view to 1
	{

		FOV = 1.0f;
	}

	Math::Vector3 movement;

	if (GetAsyncKeyState('W'))
	{
		movement.z = cameraMoveSpeed + speed;
	}
	
	if (GetAsyncKeyState('S'))
	{
		movement.z = -cameraMoveSpeed - speed;
	}

	if (GetAsyncKeyState('D'))
	{
		movement.x = cameraMoveSpeed + speed;
	}

	if (GetAsyncKeyState('A'))
	{
		movement.x = -cameraMoveSpeed - speed;
	}

	if (GetAsyncKeyState('E'))
	{
		mCamera.Rise(cameraMoveSpeed * 0.1f);
	}

	if (GetAsyncKeyState('Q'))
	{
		mCamera.Fall(cameraMoveSpeed * 0.1f);
	}
	


	mCamera.Walk(movement.z * 0.1f); // forward backward
	mCamera.Strafe(movement.x * 0.1f); // left right

	mSphereVertexShader.Bind();
	mSpherePixelShader.Bind();

	// Skydome
	/*----------------------------------------------------------------------------------------------------------------*/

	mSkyDomeTexture.BindPS(0);

	Math::Matrix TRANSLATION;
	Math::Matrix WORLD;
	Math::Matrix SCALE;
	Math::Matrix Matrices[3];

	TRANSLATION = Math::Matrix::Translation(0, 0, 0);
	SCALE = Math::Matrix::Scaling(5000.0f);
	WORLD = TRANSLATION * SCALE;

	Matrices[0] = Math::Transpose(WORLD);
	Matrices[1] = Math::Transpose(viewMatrix);
	Matrices[2] = Math::Transpose(projectionMatrix);
	mConstantBuffer.Set(Matrices);
	mConstantBuffer.BindVS();

	mSkyDomeBuffer.Draw();

	/*----------------------------------------------------------------------------------------------------------------*/

	// Sun
	/*----------------------------------------------------------------------------------------------------------------*/

	mSunTexture.BindPS(0);

	Math::Matrix TRANSLATION_0;
	Math::Matrix WORLD_0;
	Math::Matrix SCALE_0;
	Math::Matrix Matrices_0[3];

	TRANSLATION_0 = Math::Matrix::Translation(0, 0, 0);
	SCALE_0 = Math::Matrix::Scaling(100.0f);
	WORLD_0 = Math::Matrix::RotationY(rotationY) * Math::Matrix::RotationX(rotationX) * TRANSLATION_0 * SCALE_0;

	Matrices_0[0] = Math::Transpose(WORLD_0);
	Matrices_0[1] = Math::Transpose(viewMatrix);
	Matrices_0[2] = Math::Transpose(projectionMatrix);

	mConstantBuffer.Set(Matrices_0);
	mConstantBuffer.BindVS();

	mSunBuffer.Draw();

	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 1
	/*----------------------------------------------------------------------------------------------------------------*/
	mMercuryTexture.BindPS(0);

	Math::Matrix TRANSLATION_1;
	Math::Matrix WORLD_1;
	Math::Matrix SCALE_1;
	Math::Matrix Matrices_1[3];
	
	float MercuryRotationX = cos(alpha1 *Engine::Math::kDegToRad);
	float MercuryRotationZ = sin(alpha1 *Engine::Math::kDegToRad);

	TRANSLATION_1 = Math::Matrix::Translation(MercuryRotationX * 10, 0, MercuryRotationZ * 10);
	SCALE_1 = Math::Matrix::Scaling(15.0f);
	WORLD_1 = Math::Matrix::RotationY(rotationY) * Math::Matrix::RotationX(rotationX) * TRANSLATION_1 * SCALE_1;

	Matrices_1[0] = Math::Transpose(WORLD_1);
	Matrices_1[1] = Math::Transpose(viewMatrix);
	Matrices_1[2] = Math::Transpose(projectionMatrix);

	mConstantBuffer.Set(Matrices_1);
	mConstantBuffer.BindVS();


	mMercuryBuffer.Draw();

	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 2
	/*----------------------------------------------------------------------------------------------------------------*/

	mVenusTexture.BindPS(0);

	Math::Matrix TRANSLATION_2;
	Math::Matrix WORLD_2;
	Math::Matrix SCALE_2;
	Math::Matrix Matrices_2[3];

	float VenusRotationX = cos(alpha2 *Engine::Math::kDegToRad);
	float VenusRotationZ = sin(alpha2 *Engine::Math::kDegToRad);

	TRANSLATION_2 = Math::Matrix::Translation(VenusRotationX * 20, 0, VenusRotationZ * 20);
	SCALE_2 = Math::Matrix::Scaling(15.0f);
	WORLD_2 = Math::Matrix::RotationY(rotationY) * Math::Matrix::RotationX(rotationX) * TRANSLATION_2 * SCALE_2;

	Matrices_2[0] = Math::Transpose(WORLD_2);
	Matrices_2[1] = Math::Transpose(viewMatrix);
	Matrices_2[2] = Math::Transpose(projectionMatrix);


	mConstantBuffer.Set(Matrices_2);
	mConstantBuffer.BindVS();

	mVenusBuffer.Draw();

	/*----------------------------------------------------------------------------------------------------------------*/
	
	// Planet 3
	/*----------------------------------------------------------------------------------------------------------------*/

	mEarthTexture.BindPS(0);

	Math::Matrix TRANSLATION_3;
	Math::Matrix WORLD_3;
	Math::Matrix SCALE_3;
	Math::Matrix Matrices_3[3];

	float EarthRotationX = cos(alpha3 *Engine::Math::kDegToRad);
	float EarthRotationZ = sin(alpha3 *Engine::Math::kDegToRad);

	TRANSLATION_3 = Math::Matrix::Translation(EarthRotationX * 30, 0, EarthRotationZ * 30);
	SCALE_3 = Math::Matrix::Scaling(15.0f);
	WORLD_3 = Math::Matrix::RotationY(rotationY)*Math::Matrix::RotationX(rotationX)* TRANSLATION_3 * SCALE_3;

	Matrices_3[0] = Math::Transpose(WORLD_3);
	Matrices_3[1] = Math::Transpose(viewMatrix);
	Matrices_3[2] = Math::Transpose(projectionMatrix);

	mConstantBuffer.Set(Matrices_3);
	mConstantBuffer.BindVS();

	mEarthBuffer.Draw();

	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 4
	/*----------------------------------------------------------------------------------------------------------------*/

	mMarsTexture.BindPS(0);

	Math::Matrix TRANSLATION_4;
	Math::Matrix WORLD_4;
	Math::Matrix SCALE_4;
	Math::Matrix Matrices_4[3];

	float MarsRotationX = cos(alpha4 *Engine::Math::kDegToRad);
	float MarsRotationZ = sin(alpha4 *Engine::Math::kDegToRad);

	TRANSLATION_4 = Math::Matrix::Translation(MarsRotationX * 40, 0, MarsRotationZ * 40);
	SCALE_4 = Math::Matrix::Scaling(15.0f);
	WORLD_4 = Math::Matrix::RotationY(rotationY) * Math::Matrix::RotationX(rotationX) * TRANSLATION_4 * SCALE_4;

	Matrices_4[0] = Math::Transpose(WORLD_4);
	Matrices_4[1] = Math::Transpose(viewMatrix);
	Matrices_4[2] = Math::Transpose(projectionMatrix);

	mConstantBuffer.Set(Matrices_4);
	mConstantBuffer.BindVS();

	mMarsBuffer.Draw();

	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 5
	/*----------------------------------------------------------------------------------------------------------------*/

	mJupiterTexture.BindPS(0);

	Math::Matrix TRANSLATION_5;
	Math::Matrix WORLD_5;
	Math::Matrix SCALE_5;
	Math::Matrix Matrices_5[3];

	float JupiterRotationX = cos(alpha5 *Engine::Math::kDegToRad);
	float JupiterRotationZ = sin(alpha5 *Engine::Math::kDegToRad);

	TRANSLATION_5 = Math::Matrix::Translation(JupiterRotationX * 15, 0, JupiterRotationZ * 15);
	SCALE_5 = Math::Matrix::Scaling(50.0f);
	WORLD_5 = Math::Matrix::RotationY(rotationY) * Math::Matrix::RotationX(rotationX) * TRANSLATION_5 * SCALE_5;

	Matrices_5[0] = Math::Transpose(WORLD_5);
	Matrices_5[1] = Math::Transpose(viewMatrix);
	Matrices_5[2] = Math::Transpose(projectionMatrix);

	mConstantBuffer.Set(Matrices_5);
	mConstantBuffer.BindVS();

	mJupiterBuffer.Draw();

	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 6
	/*----------------------------------------------------------------------------------------------------------------*/

	mSaturnTexture.BindPS(0);

	Math::Matrix TRANSLATION_6;
	Math::Matrix WORLD_6;
	Math::Matrix SCALE_6;
	Math::Matrix Matrices_6[3];

	float SaturnRotationX = cos(alpha6 *Engine::Math::kDegToRad);
	float SaturnRotationZ = sin(alpha6 *Engine::Math::kDegToRad);

	TRANSLATION_6 = Math::Matrix::Translation(SaturnRotationX * 23, 0, SaturnRotationZ * 23);
	SCALE_6 = Math::Matrix::Scaling(40.0f);
	WORLD_6 = Math::Matrix::RotationY(rotationY) * Math::Matrix::RotationX(rotationX) * TRANSLATION_6 * SCALE_6;

	Matrices_6[0] = Math::Transpose(WORLD_6);
	Matrices_6[1] = Math::Transpose(viewMatrix);
	Matrices_6[2] = Math::Transpose(projectionMatrix);

	mConstantBuffer.Set(Matrices_6);
	mConstantBuffer.BindVS();

	mSaturnBuffer.Draw();

	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 7
	/*----------------------------------------------------------------------------------------------------------------*/

	mUranusTexture.BindPS(0);

	Math::Matrix TRANSLATION_7;
	Math::Matrix WORLD_7;
	Math::Matrix SCALE_7;
	Math::Matrix Matrices_7[3];

	float UranusRotationX = cos(alpha7 *Engine::Math::kDegToRad);
	float UranusRotationZ = sin(alpha7 *Engine::Math::kDegToRad);

	TRANSLATION_7 = Math::Matrix::Translation(UranusRotationX * 43, 0, UranusRotationZ * 43);
	SCALE_7 = Math::Matrix::Scaling(25.0f);
	WORLD_7 = Math::Matrix::RotationY(rotationY) * Math::Matrix::RotationX(rotationX) * TRANSLATION_7 * SCALE_7;

	Matrices_7[0] = Math::Transpose(WORLD_7);
	Matrices_7[1] = Math::Transpose(viewMatrix);
	Matrices_7[2] = Math::Transpose(projectionMatrix);

	mConstantBuffer.Set(Matrices_7);
	mConstantBuffer.BindVS();

	mUranusBuffer.Draw();

	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 8
	/*----------------------------------------------------------------------------------------------------------------*/

	mNeptuneTexture.BindPS(0);

	Math::Matrix TRANSLATION_8;
	Math::Matrix WORLD_8;
	Math::Matrix SCALE_8;
	Math::Matrix Matrices_8[3];

	float NeptuneRotationX = cos(alpha8 *Engine::Math::kDegToRad);
	float NeptuneRotationZ = sin(alpha8 *Engine::Math::kDegToRad);

	TRANSLATION_8 = Math::Matrix::Translation(NeptuneRotationX * 80, 0, NeptuneRotationZ * 80);
	SCALE_8 = Math::Matrix::Scaling(15.0f);
	WORLD_8 = Math::Matrix::RotationY(rotationY) * Math::Matrix::RotationX(rotationX) * TRANSLATION_8 * SCALE_8;

	Matrices_8[0] = Math::Transpose(WORLD_8);
	Matrices_8[1] = Math::Transpose(viewMatrix);
	Matrices_8[2] = Math::Transpose(projectionMatrix);

	mConstantBuffer.Set(Matrices_8);
	mConstantBuffer.BindVS();

	mNeptuneBuffer.Draw();

	/*----------------------------------------------------------------------------------------------------------------*/

	// Planet 9
	/*----------------------------------------------------------------------------------------------------------------*/

	mPlutoTexture.BindPS(0);

	Math::Matrix TRANSLATION_9;
	Math::Matrix WORLD_9;
	Math::Matrix SCALE_9;
	Math::Matrix Matrices_9[3];

	float PlutoRotationX = cos(alpha9 *Engine::Math::kDegToRad);
	float PlutoRotationZ = sin(alpha9 *Engine::Math::kDegToRad);

	TRANSLATION_9 = Math::Matrix::Translation(PlutoRotationX * 140, 0, PlutoRotationZ * 140);
	SCALE_9 = Math::Matrix::Scaling(10.0f);
	WORLD_9 = Math::Matrix::RotationY(rotationY) * Math::Matrix::RotationX(rotationX) * TRANSLATION_9 * SCALE_9;

	Matrices_9[0] = Math::Transpose(WORLD_9);
	Matrices_9[1] = Math::Transpose(viewMatrix);
	Matrices_9[2] = Math::Transpose(projectionMatrix);

	mConstantBuffer.Set(Matrices_9);
	mConstantBuffer.BindVS();

	mPlutoBuffer.Draw();

	/*----------------------------------------------------------------------------------------------------------------*/

	Graphics::GraphicsSystem::Get()->EndRender();
}
