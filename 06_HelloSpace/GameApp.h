#ifndef INCLUDED_GAMEAPP_H
#define INCLUDED_GAMEAPP_H

#include <Core/Inc/Core.h>
#include <Graphics/Inc/Graphics.h>
#include <Input/Inc/Input.h>
#include <Math/Inc/EngineMath.h>

class GameApp : public Engine::Core::Application
{
public:
	GameApp();
	~GameApp() override;

private:
	void OnInitialize() override;
	void OnTerminate() override;
	void OnUpdate() override;

	Engine::Core::Window mWindow;
	Engine::Input::InputSystem mInput;
	Engine::Graphics::Camera mCamera;
	Engine::Graphics::ConstantBuffer mConstantBuffer;

	Engine::Graphics::MeshS mSkyDome;
	Engine::Graphics::MeshBuilder mSkyDomeBuilder;
	Engine::Graphics::MeshBuffer mSkyDomeBuffer;
	Engine::Graphics::VertexShader mSkyDomeVertexShader;
	Engine::Graphics::PixelShader mSkyDomePixelShader;
	Engine::Graphics::Texture mSkyDomeTexture;

	Engine::Graphics::MeshS mSphere;
	Engine::Graphics::MeshBuilder mSphereBuilder;
	Engine::Graphics::MeshBuffer mSphereBuffer;
	Engine::Graphics::VertexShader mSphereVertexShader;
	Engine::Graphics::PixelShader mSpherePixelShader;
	Engine::Graphics::Texture mSphereTexture;

	Engine::Graphics::MeshS mSun;
	Engine::Graphics::MeshBuilder mSunBuilder;
	Engine::Graphics::MeshBuffer mSunBuffer;
	Engine::Graphics::Texture mSunTexture;

	Engine::Graphics::MeshS mMercury;
	Engine::Graphics::MeshBuilder mMercuryBuilder;
	Engine::Graphics::MeshBuffer mMercuryBuffer;
	Engine::Graphics::Texture mMercuryTexture;

	Engine::Graphics::MeshS mVenus;
	Engine::Graphics::MeshBuilder mVenusBuilder;
	Engine::Graphics::MeshBuffer mVenusBuffer;
	Engine::Graphics::Texture mVenusTexture;

	Engine::Graphics::MeshS mEarth;
	Engine::Graphics::MeshBuilder mEarthBuilder;
	Engine::Graphics::MeshBuffer mEarthBuffer;
	Engine::Graphics::Texture mEarthTexture;

	Engine::Graphics::MeshS mMars;
	Engine::Graphics::MeshBuilder mMarsBuilder;
	Engine::Graphics::MeshBuffer mMarsBuffer;
	Engine::Graphics::Texture mMarsTexture;

	Engine::Graphics::MeshS mJupiter;
	Engine::Graphics::MeshBuilder mJupiterBuilder;
	Engine::Graphics::MeshBuffer mJupiterBuffer;
	Engine::Graphics::Texture mJupiterTexture;

	Engine::Graphics::MeshS mSaturn;
	Engine::Graphics::MeshBuilder mSaturnBuilder;
	Engine::Graphics::MeshBuffer mSaturnBuffer;
	Engine::Graphics::Texture mSaturnTexture;
	
	Engine::Graphics::MeshS mUranus;
	Engine::Graphics::MeshBuilder mUranusBuilder;
	Engine::Graphics::MeshBuffer mUranusBuffer;
	Engine::Graphics::Texture mUranusTexture;

	Engine::Graphics::MeshS mNeptune;
	Engine::Graphics::MeshBuilder mNeptuneBuilder;
	Engine::Graphics::MeshBuffer mNeptuneBuffer;
	Engine::Graphics::Texture mNeptuneTexture;

	Engine::Graphics::MeshS mPluto;
	Engine::Graphics::MeshBuilder mPlutoBuilder;
	Engine::Graphics::MeshBuffer mPlutoBuffer;
	Engine::Graphics::Texture mPlutoTexture;

	// for planet rotation
	float rotationY = 0.0f;
	float rotationX = 0.0f;

	// switch for rotation and orbiting
	bool Switch = false;
	bool Switch1 = false;
	bool Switch2 = false;

	// field of view
	float FOV = 1.0f;

	// for planet orbiting speed
	float alpha1 = 0;
	float alpha2 = 0;
	float alpha3 = 0;
	float alpha4 = 0;
	float alpha5 = 0;
	float alpha6 = 0;
	float alpha7 = 0;
	float alpha8 = 0;
	float alpha9 = 0;
};

#endif //#ifndef INCLUDED_GAMEAPP_H