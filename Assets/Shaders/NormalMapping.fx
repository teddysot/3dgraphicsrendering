cbuffer ConstantBuffer : register(b0)
{		
	matrix wvp;
	matrix World;
	float3 viewPosition;
	float3 lightDirection;
	float4 lightAmbient;
	float4 lightDiffuse;
	float4 lightSpecular;
	float4 materialAmbient;
	float4 materialDiffuse;
	float4 materialSpecular;
	float power;

}

struct VS_INPUT
{
	float4 position	: POSITION;
	float3 normal 	: NORMAL;
	float3 tangent : TANGENT;
	float4 color : COLOR;
	float2 texCoord	: TEXCOORD;
};

struct VS_OUTPUT
{
	float4 position	: SV_POSITION;
	float3 world	: TEXCOORD0;
	float3 normal	: TEXCOORD1;
	float3 tangent : TEXTCOORD2;
	float2 texCoord : TEXTCOORD3;

	float3 binormal : BINORMAL;

	//float4 position : SV_POSITION;
	//float3 normal : NORMAL;
	//float3 tangent : TANGENT;
	//float3 binormal : BINORMAL;
	//float3 dirToLight : TEXCOORD0;
	//float3 dirToView : TEXCOORD1;
	//float2 texCoord : TEXCOORD2;



	//float4 color	: TEXCOORD3;
	//float3 dirToLight : TEXCOORD4;
	//float3 dirToView : TEXCOORD5;


	//float4 position		: SV_POSITION;
	//float3 normal		: TEXCOORD0;
	//float3 dirToLight	: TEXCOORD1;
	//float3 dirToView	: TEXCOORD2;
	//float2 texCoord		: TEXCOORD3;

};

Texture2D diffuseMap  : register(t0);
Texture2D SpecMap : register(t1);
Texture2D DarkMap : register(t2);
Texture2D NormalMap : register(t3);
Texture2D BumpMap : register(t4);
SamplerState textureSampler : register(s0);

//====================================================================================================
// Vertex Shader
//====================================================================================================

VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	output.position = mul(input.position, wvp);
	output.world = mul(input.position, World);
	output.normal = mul(input.normal, World);
	output.texCoord = input.texCoord;
	output.binormal = mul(input.normal, input.tangent);
	//output.color = input.lightAmbient;
	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VS_OUTPUT input) : SV_Target
{		



	float bumpLevel = BumpMap.Sample(textureSampler, input.texCoord);
	float specAmount = SpecMap.Sample(textureSampler, input.texCoord);
	float4 darkDiffuseTexture = DarkMap.Sample(textureSampler, input.texCoord);
	float4 normalTexture = NormalMap.Sample(textureSampler, input.texCoord);

	

	float4 ambientColor = lightAmbient * materialAmbient;

	float3 normal = normalize(input.normal);
	float3 dirToLight = -lightDirection;
	float3 dirToView = normalize(viewPosition - input.world);
	float diffuse = max(0, (dot(dirToLight, normal)))* bumpLevel;
	float4 diffuseColor = diffuse * lightDiffuse * materialDiffuse;
	
	float3 halfVector = normalize((dirToLight + dirToView) * 0.5f);
	float4 normalColor = NormalMap.Sample(textureSampler, input.texCoord); //
	float3 sampledNormal = float3((2.0f * normalColor.xy) - 1.0f, normalColor.z); //


	float specular = max(0,(dot(halfVector, normal)))* (normalColor);
	float spe = max(0, (dot(halfVector, normal)));//
	float4 specularColor = pow(specular, power) * lightSpecular * materialSpecular;

	float4 diffuseTexture = diffuseMap.Sample(textureSampler, input.texCoord);

	float dis = max(0, (dot(sampledNormal, normal)))* bumpLevel; //

	float4 textureMiddle = darkDiffuseTexture + specular * (diffuseTexture - darkDiffuseTexture);



	return (ambientColor + diffuseColor) * textureMiddle + (specularColor * specAmount);

//	return (ambientColor + diffuseColor + specularColor) * diffuseTexture/* * diffuseTexture*/;
	//float4 textureColor = diffuseMap.Sample(textureSampler, input.texCoord);
	////float4 lightingColor = input.color;
	//return textureColor;
}


//https://learnopengl.com/Lighting/Basic-Lighting
//http://in2gpu.com/2014/06/19/lighting-vertex-fragment-shader/