cbuffer ConstantBuffer : register(b0)
{
	matrix wvp;
	matrix world;
	float3 viewPosition;		// World space
	float3 lightDirection;		// World space
	float4 lightAmbient;
	float4 lightDiffuse;
	float4 lightSpecular;
	float4 materialAmbient;
	float4 materialDiffuse;
	float4 materialSpecular;
	float power;
}

struct VS_INPUT
{
	float4 position	: POSITION;
	float3 normal 	: NORMAL;
	float3 tangent	: TANGENT;
	float4 color	: COLOR;
	float2 texcoord	: TEXCOORD;
};

struct VS_OUTPUT
{
	float4 position	: SV_POSITION;
	float3 worldPos	: TEXCOORD0;
	float3 normal	: TEXCOORD1;
	float3 tangent	: TEXCOORD2;
	float2 texcoord	: TEXCOORD3;
};

Texture2D diffuseMap  : register(t0);
SamplerState textureSampler : register(s0);

//====================================================================================================
// Vertex Shader
//====================================================================================================

VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	output.position = mul(input.position, wvp);
	output.worldPos = mul(input.position, world);
	output.normal = mul(float4(input.normal, 0.0f), world);
	output.texcoord = input.texcoord;
	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VS_OUTPUT input) : SV_Target
{
	// Re-normalize normals
	float3 normal = normalize(input.normal);
	float3 dirToLight = -lightDirection;
	float3 dirToView = normalize(viewPosition - input.worldPos);

	// Ambient
	float4 ambientColor = lightAmbient * materialAmbient;

	// Diffuse
	float diffuse = saturate(dot(dirToLight, normal));
	float4 diffuseColor = diffuse * lightDiffuse * materialDiffuse;

	// Specular
	float3 halfVector = normalize((dirToLight + dirToView) * 0.5f);
	float specular = saturate(dot(halfVector, normal));
	float4 specularColor = pow(specular, power) * lightSpecular * materialSpecular;

	// Texture values
	float4 diffuseTexture = diffuseMap.Sample(textureSampler, input.texcoord);

	// Final color
	return (ambientColor + diffuseColor) * diffuseTexture + (specularColor);
}
