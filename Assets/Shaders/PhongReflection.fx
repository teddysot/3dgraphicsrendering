//====================================================================================================
// Filename:	Standard.fx
// Created by:	Peter Chan
// Description: Standard shader.
//====================================================================================================

cbuffer ConstantBuffer : register(b0)
{
	matrix wvp;
	matrix World;
	float3 viewPosition;
	float3 lightDirection;
	float4 lightAmbient;
	float4 lightDiffuse;
	float4 lightSpecular;
	float4 materialAmbient;
	float4 materialDiffuse;
	float4 materialSpecular;
	float power;
}

struct VS_INPUT
{
	float4 position	: POSITION;
	float3 normal 	: NORMAL;
	float2 texCoord	: TEXCOORD;
};

struct VS_OUTPUT
{
	float4 position	: SV_POSITION;
	float3 world	: TEXCOORD0;
	float3 normal	: TEXCOORD1;
	float2 texCoord	: TEXCOORD2;


	//float4 color	: TEXCOORD3;
	//float3 dirToLight : TEXCOORD4;
	//float3 dirToView : TEXCOORD5;


	/*float4 position		: SV_POSITION;
	float3 normal		: TEXCOORD0;
	float3 dirToLight	: TEXCOORD1;
	float3 dirToView	: TEXCOORD2;
	float2 texCoord		: TEXCOORD3;*/

};

Texture2D diffuseMap  : register(t0);
SamplerState textureSampler : register(s0);

//====================================================================================================
// Vertex Shader
//====================================================================================================

VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	output.position = mul(input.position, wvp);
	output.world = mul(input.position, World);
	output.normal = mul(input.normal, World);
	output.texCoord = input.texCoord;
	//	output.color = input.lightAmbient;
	return output;
}

//====================================================================================================
// Pixel Shader
//====================================================================================================

float4 PS(VS_OUTPUT input) : SV_Target
{
	float4 ambientColor = lightAmbient * materialAmbient;

	float3 normal = normalize(input.normal);
	float3 dirToLight = -lightDirection;
	float3 dirToView = normalize(viewPosition - input.world);
	float diffuse = max(0, (dot(dirToLight, normal)));
	float4 diffuseColor = diffuse * lightDiffuse * materialDiffuse;

	float3 halfVector = normalize((dirToLight + dirToView));
	float specular = max(0,(dot(halfVector, normal)));
	float4 specularColor = pow(specular, power) * lightSpecular * materialSpecular;

	float4 diffuseTexture = diffuseMap.Sample(textureSampler, input.texCoord);

	return (ambientColor + diffuseColor + specularColor) * diffuseTexture/* * diffuseTexture*/;


	//float4 textureColor = diffuseMap.Sample(textureSampler, input.texCoord);
	////float4 lightingColor = input.color;
	//return textureColor;
}


//https://learnopengl.com/Lighting/Basic-Lighting
//http://in2gpu.com/2014/06/19/lighting-vertex-fragment-shader/