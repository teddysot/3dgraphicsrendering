#ifndef INCLUDE_CORE_APPLICATION_H
#define INCLUDE_CORE_APPLICATION_H

namespace Engine {
namespace Core {

	class Application
	{
	public:
		Application();
		virtual ~Application();

		Application(const Application&) = delete; //dont want copy behav   ,dont want your application to even be copied
		Application& operator =(const Application&) = delete; //dont want assign behav

		void Initialize(HINSTANCE instance, LPCSTR appName);
		void Terminate();

		void HookWindow(HWND hWnd);
		void UnhookWindow();

		void Update();

		void Kill()							{ mRunning = false; }



		HINSTANCE GetInstance() const		{ return mInstance; }
		HWND GetWindow() const				{ return mWindow; }
		const char* GetAppName() const		{ return mAppName.c_str(); }
		bool IsRunning() const				{ return mRunning; }

private:
	virtual void OnInitialize() = 0;
	virtual void OnTerminate() = 0;
	virtual void OnUpdate() = 0;

	HINSTANCE mInstance;
	HWND mWindow;
	std::string mAppName;
	bool mRunning;
};

}	// namespace Core
}	// namespace Engine

#endif


/*
special member functions

These are class methods that c++ will generate for you automatically

class Foo
{
public:
	Foo() {};							constructor
	Foo (const Foo&) {};				copy constructor		 [shallow copy eveyting]
	~Foo() {};							destructor
	Foo& operator=(const Foo&);			assignment operator		[shallow copy eveyting]


	Foo(Foo&) {};						move					[move everything]
	Foo& operator=(Foo&);				move operator			[move eveyting]
};


void main()
{
	Foo foo;
	Foo bar(foo);
	bar = foo = bar;
	Foo car = std::move(bar);
	bar = std::move(foo);

}

if class is a pointer, shallow copy is a bad idea





private::
template method pattern
	virtual void
*/