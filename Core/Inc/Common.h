#ifndef INCLUDED_CORE_COMMON_H
#define INCLUDED_CORE_COMMON_H

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX

// Win32 headers
#include <objbase.h>
#include <Windows.h>

// Standard headers
#include <algorithm>
#include <array>
#include <cstdio>
#include <cstdint>
#include <functional>
#include <list>
#include <map>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#endif // #ifndef INCLUDED_CORE_COMMON_H