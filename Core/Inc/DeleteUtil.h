#ifndef INCLUDED_CORE_DELETEUTIL_H
#define INCLUDED_CORE_DELETEUTIL_H

template <class T>
inline void SafeDelete(T*& ptr)
{
	delete ptr;
	ptr = nullptr;
}

template <class T>
inline void SafeDeleteArray(T*& ptr)
{
	delete[] ptr;
	ptr = nullptr;
}

template <class T>
inline void SafeRelease(T*& ptr)
{
	if (ptr)
	{
		ptr->Release();
		ptr = nullptr;
	}
}


#endif // #ifndef INCLUDED_CORE_DELETEUTIL_H

/*
void SafeDelete(<type>& ptr)

template <class T>
void SafeDelete(T*& ptr) // reference

void SafeDelete(T*& ptr) // reference
{
	delete[] ptr;
	ptr[] = nulltpr;
}

void SafeDelete(Car*& ptr) // reference
{
	delete ptr;
	ptr = nulltpr; // passing pointer to the stack
}

void main(int argc, char* argv[])
{
	int* myArray = new int[10]; // theres no delete - memory leak
	delete[] myArray; //for array
	delete myArray; for new int;

	Car* ptr = new Car[10];
	delete[] ptr;  .... gave the memory back but still holding on to the address .... dangling pointer
	ptr = nulltpr; memory stop ... always want to do it






	<-20->
	[car][car][car][car][car][car][car]
	^
	| 0

	ptr[0]; // subscript operator
	//does pointer math
	int n = 42;
	n += 1; // 42 + 1 = 43;
	int* p = 42;
	p += 1; // 42 + (1 * sizeof(Car)) 42;  .... int = 4

	Car * cp 42;
	cp += 1; 42 + (1 * sizeof(Car)) 62;


	return 0;
}
*/