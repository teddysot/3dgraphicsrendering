#ifndef INCLUDE_CORE_WINDOW_H
#define INCLUDE_CORE_WINDOW_H

namespace Engine {
namespace Core {

class Window
{
public:
	Window();
	~Window();

	void Initialize(HINSTANCE instance, LPCSTR appName, uint32_t width, uint32_t height);
	void Terminate();

	bool ProcessMessage(); //function that looks at all the messeges

	HWND GetWindowHandle() const { return mWindow; }

private:
	HINSTANCE mInstance;
	HWND mWindow;
	std::string mAppName;
	//int Num;
	//void(*myFunc)(int); // define a member that takes a function pointer


};

}	// namespace Core
}	// namespace Engine

using WindowMessengeHandler = LRESULT(CALLBACK*)(HWND, UINT, WPARAM, LPARAM);

#endif




/*
1) Create window class
2) Create an instance of your window class
3) Show window

What is Callable?
something that an be invoked via the () (function invocation operator)

i) Function Pointers
void Foo(int) {}


ii) Functor
struct Footer
{
	void operator()(int) {};
};


iii) Lambda
auto func = [](int) {};

void main()
{
	std::function<void(int)>myCallable;
	myCallable = Foo;
	myCallable = Footer();
	myCallable = func;
	myCallable(42);
}

*/