#ifndef INCLUDED_GRAPHICS_CAMERA_H
#define INCLUDED_GRAPHICS_CAMERA_H

namespace Engine {
namespace Graphics {

// This camera assumes y-axis as up direction always
class Camera
{
public:
	Camera();
	//~Camera();

	void SetPosition(const Math::Vector3& position);
	void SetDirection(const Math::Vector3& direction);
	void SetLookAt(const Math::Vector3& target); // vector subtraction

	void Walk(float distance); // changing position  compute a new vector, add some multiple of your direction
	void Strafe(float distance); //cross product
	void Rise(float distance); // update position by the y axis
	void Fall(float distance); 

	void Yaw(float radian); // do rotation
	void Pitch(float radian);

	void SetFOV(float fov);
	void SetNearPlane(float nearPlane);
	void SetFarPlane(float farPlane);

	// TO DO
	Math::Vector3 GetPosition()const { return mPosition; }
	Math::Vector3 GetDirection()const { return mDirection; }
	float GetFov() const { return mFOV; }
	float GetNearPlane() const { return mNearPlane; }
	float GetFarPlane() const { return mFarPlane; }


	Math::Matrix GetViewMatrix() const;
	Math::Matrix GetPerspectiveMatrix(float aspectRatio = 0.0f) const;
	Math::Matrix GetProjectionMatrix(uint32_t screenWidth, uint32_t screenHeight) const;
	Math::Matrix GetWorldMatrix() const;

private:
	Math::Vector3 mPosition;
	Math::Vector3 mDirection;
	Math::Quaternion mRotation;
	Math::Vector3 rotate;

	float mFOV;
	float mNearPlane;
	float mFarPlane;
};


} // namespace Graphics
} // namespace Engine

#endif 