#ifndef ENGINE_GRAPHICS_LIGHT_H
#define ENGINE_GRAPHICS_LIGHT_H

#include <Math\Inc\Vector4.h>
#include <Math\Inc\Vector3.h>
namespace Engine {
namespace Graphics {

struct DirectionalLight
{
	DirectionalLight()
		: direction(0.0f, 0.0f, 1.0f)
		, ambient(1.0f,1.0f,1.0f,1.0f)
		, diffuse(1.0f, 1.0f, 1.0f, 1.0f)
		, specular(1.0f, 1.0f, 1.0f, 1.0f)
	{}


	Engine::Math::Vector3 direction;
	float padding = 0.0f;
	Engine::Math::Vector4 ambient;
	Engine::Math::Vector4 diffuse;
	Engine::Math::Vector4 specular;
	


};
}
}

#endif