#ifndef INCLUDED_GRAPHICS_GRAPHICSSYSTEM_H
#define INCLUDED_GRAPHICS_GRAPHICSSYSTEM_H

namespace Engine {
namespace Graphics {

class GraphicsSystem
{
public:
	static void StaticInitialize(HWND window, bool fullscreen);
	static void StaticTerminate();
	static GraphicsSystem* Get();

public:
	GraphicsSystem();
	~GraphicsSystem();

	void Initialize(HWND window, bool fullscreen);
	void Terminate();

	void BeginRender();
	void EndRender();

	void ToggleFullscreen();
	void ResetRenderTarget();
	void ResetViewport();

	void EnableDepthTesting(bool enable);
	
	uint32_t GetWidth() const;
	uint32_t GetHeight() const;
	float GetAspectRatio() const;

	ID3D11Device* GetDevice()			{ return mD3DDevice; }
	ID3D11DeviceContext* GetContext()	{ return mImmediateContext; }
	
private:
	GraphicsSystem(const GraphicsSystem&) = delete;
	GraphicsSystem& operator=(const GraphicsSystem&) = delete;

	ID3D11Device* mD3DDevice; // handle V ram
	ID3D11DeviceContext* mImmediateContext; // command, handle draw call

	IDXGISwapChain* mSwapChain;
	ID3D11RenderTargetView* mRenderTargetView;

	ID3D11Texture2D* mDepthStencilBuffer;
	ID3D11DepthStencilView* mDepthStencilView;
	ID3D11DepthStencilState* mDisableDepthStencil;

	D3D_DRIVER_TYPE mDriverType;
	D3D_FEATURE_LEVEL mFeatureLevel;

	DXGI_SWAP_CHAIN_DESC mSwapChainDesc;

	D3D11_VIEWPORT mViewport;
};

} // namespace Graphics
} // namespace Engine

#endif // #ifndef INCLUDED_GRAPHICS_GRAPHICSSYSTEM_H

// Our code
/*
D3D11Device* device;
device->DoThis();
device->DoThat();

*/
// directX  interface based programming
//--------------------------Direct3D---------------------------// 
/*

struct ID#D11Device
{
	virtual void DoThis() = 0;
};



class D3D11Device : public D3D11Device
{
public:
	void DoThis();
	void DoThat();

};

*/


/*
class Singleton
{
public:
	static Singleton* Get();

private:
	Singleton();
	static Singleton* instance;
};
*/