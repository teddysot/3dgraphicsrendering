#ifndef ENGINE_GRAPHICS_MATERIAL_H
#define ENGINE_GRAPHICS_MATERIAL_H

#include "Math\Inc\Vector4.h"

namespace Engine {
namespace Graphics {

struct Material
{
	Material()
		: ambient(0.0f, 0.0f, 0.0f, 0.0f)
		, diffuse(0.0f, 0.0f, 0.0f, 0.0f)
		, specular(0.0f, 0.0f, 0.0f, 0.0f)
	{}

	Engine::Math::Vector4 ambient;
	Engine::Math::Vector4 diffuse;
	Engine::Math::Vector4 specular;
	float power = 1.0f;
	

};
}
}

#endif
