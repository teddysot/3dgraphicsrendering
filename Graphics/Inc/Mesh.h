#ifndef INCLUDED_GRAPHICS_MESH_H
#define INCLUDED_GRAPHICS_MESH_H

#include "VertexTypes.h"

namespace Engine {
namespace Graphics {

	template<class VertexType>
	class BaseMesh
	{

	public:
		void Allocate(uint32_t numVertices, uint32_t numIndices)
		{
			mVertices = std::make_unique<VertexType[]>(numVertices);
			mIndices = std::make_unique<uint32_t[]>(numIndices);

			mNumVertices = numVertices;
			mNumIndices = numIndices;
		}
		void Destroy()
		{
			mVertices.reset();
			mIndices.reset();
		}

		VertexType& GetVertex(uint32_t index)
		{
			ASSERT(index < mNumVertices, "[Mesh] index out of range");
			return mVertices[index];
		}
		uint32_t& GetIndex(uint32_t index)
		{
			ASSERT(index < mNumIndices, "[Mesh] index out of range");
			return mIndices[index];
		}

		uint32_t& AddIndices(uint32_t index)
		{
			return mIndices[mIndexCount++] = index;
		}

		const VertexType* GetVertices() const{ return mVertices.get(); }
		const uint32_t* GetIndices() const { return mIndices.get(); }

		uint32_t GetVertexCountPC() const { return sizeof(Graphics::VertexPC); }
		uint32_t GetVertexCountS() const { return sizeof(Graphics::VertexS); }
		uint32_t GetVertexCountPNT() const { return sizeof(Graphics::VertexPNT); }

		uint32_t GetVertexSize() const { return mNumVertices; }
		uint32_t GetIndexCount() const { return mNumIndices; }



	private:
		std::unique_ptr<VertexType[]> mVertices;
		std::unique_ptr<uint32_t[]> mIndices;

		uint32_t mVertexSize = sizeof(Graphics::VertexPC);
		//uint32_t mVertexSize = sizeof(Graphics::VertexS);

		uint32_t mNumVertices = 0;
		uint32_t mNumIndices = 0;

		uint32_t mIndexCount = 0;
	};

	using MeshP = BaseMesh<VertexP>;
	using MeshPC = BaseMesh<VertexPC>;
	using MeshPT = BaseMesh<VertexPT>;
	using MeshS = BaseMesh<VertexS>;
	using MeshPNT = BaseMesh<VertexPNT>;


} // namespace Graphics
} // namespace Engine

#endif 