#ifndef INCLUDED_GRAPHICS_MESHBUFFER_H
#define INCLUDED_GRAPHICS_MESHBUFFER_H

#include "Mesh.h"

namespace Engine {
namespace Graphics {

class MeshBuffer
{
public:
	MeshBuffer() = default;
	void Initialize(const void* sVertices, uint32_t vertexPC, uint32_t verticesSize, uint32_t indicesSize, const uint32_t* sIndices);
	void Initialize(const MeshPC& mesh);
	void Initialize(const MeshS& mesh);
	void Initialize(const MeshPT& mesh);
	void Initialize(const MeshPNT& mesh);
	void Terminate();
	void Draw();
	void Draw(uint32_t vertexPC, uint32_t indicesSize);

private:

	ID3D11Buffer * mVertexBuffer;
	ID3D11Buffer* mIndexBuffer;


	uint32_t mVertexSize;
	uint32_t mVertexCount;
	uint32_t mIndexCount;

	D3D_PRIMITIVE_TOPOLOGY mTopology;
};


} // namespace Graphics
} // namespace Engine

#endif 