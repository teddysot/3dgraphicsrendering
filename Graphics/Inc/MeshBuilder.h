#ifndef INCLUDED_GRAPHICS_MESHBUILDER_H
#define INCLUDED_GRAPHICS_MESHBUILDER_H

#include "Mesh.h"

namespace Engine {
namespace Graphics {

class MeshBuilder
{

public:


	/*For Homework

	 Add HelloTexture project
	 Add new vertex type "VertexPT", which will have position and uv
	 Add helper function to MeshBuilder to generate different meshes
	 Create a scene in HelloTexture using all the meshes you created
	 
	 
	 */

	static void GenerateCubePC(MeshPC& mesh);
	static void GenerateCubePT(MeshPT& mesh);
	static void GeneratePyramidPT(MeshPT& mesh);
	static void GenerateSphereS(MeshS& mesh, uint32_t rings, uint32_t slices);
	static void GenerateSpherePNT(MeshPNT& mesh, uint32_t rings, uint32_t slices);
	static void GenerateSkydomeS(MeshS& mesh, uint32_t rings, uint32_t slices);

};

} // namespace Graphics
} // namespace Engine

#endif 