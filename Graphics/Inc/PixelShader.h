#ifndef INCLUDED_GRAPHICS_PIXELSHADER_H
#define INCLUDED_GRAPHICS_PIXELSHADER_H


namespace Engine {
namespace Graphics {

class PixelShader
{
public:
	PixelShader() = default;
	void Initialize(const wchar_t* filename);
	void Terminate();
	void Bind();


private:

	ID3D11PixelShader* mPixelShader;

};
} // namespace Engine
} // namespace Graphics

#endif //#endif INCLUDED_GRAPHICS_PIXELSHADER_H