#ifndef INCLUDED_GRAPHICS_SIMPLEDRAW_H
#define INCLUDED_GRAPHICS_SIMPLEDRAW_H

namespace Engine {
namespace Graphics {

class SimpleDraw
{
public:
	// Functions to startup/shutdown simple draw
	void Initialize(uint32_t maxVertices);
	void Terminate();

	// Functions for world space rendering
	void AddLine(const Math::Vector3& v0, const Math::Vector3& v1, const Math::Vector4& color);

	//void Render(const Engine::Graphics::Camera& camera);
};
} // namespace Graphics
} // namespace Engine

#endif