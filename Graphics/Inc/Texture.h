#ifndef INCLUDED_GRAPHICS_TEXTURE_H
#define INCLUDED_GRAPHICS_TEXTURE_H

namespace Engine {
namespace Graphics {

class Texture
{
public:
	Texture();
	~Texture();

	bool Initialize(const char* fileName);
	void Initialize(const void* data, uint32_t width, uint32_t height);
	void Terminate();

	void Set(const void* data, uint32_t size);

	void BindVS(uint32_t slot) const;
	void BindPS(uint32_t slot) const;
	
private:
	Texture(const Texture&) = delete;
	Texture& operator=(const Texture&) = delete;

	friend class SpriteRenderer;

	ID3D11ShaderResourceView* mShaderResourceView;
	D3D11_TEXTURE2D_DESC mTextureDescription;
};

} // namespace Graphics
} // namespace Engine

#endif // #ifndef INCLUDED_GRAPHICS_TEXTURE_H