#ifndef INCLUDED_GRAPHICS_VERTEXSHADER_H
#define INCLUDED_GRAPHICS_VERTEXSHADER_H


namespace Engine {
namespace Graphics {

class VertexShader 
{
public:

	VertexShader() = default;
	void Initialize(const wchar_t* filename, int vertexFormat);
	void Terminate();
	void Bind();

private:

	ID3D11VertexShader* mVertexShader{ nullptr };
	ID3D11InputLayout* mInputLayout{ nullptr };

};
} // namespace Engine
} // namespace Graphics
#endif