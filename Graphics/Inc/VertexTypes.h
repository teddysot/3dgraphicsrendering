#ifndef INCLUDED_GRAPHICS_VERTEXTYPES_H
#define INCLUDED_GRAPHICS_VERTEXTYPES_H

#include "Common.h"

namespace Engine {
namespace Graphics {


struct VertexP
{
	static constexpr int Format = 0;
	Math::Vector3 position;

};

struct VertexPC
{
	static constexpr int Format = 1;
	Engine::Math::Vector3 position;
	//float w, x, y, z;
	Engine::Math::Vector4 color;
};


struct VertexPT
{
	static constexpr int Format = 2;
	Engine::Math::Vector3 position;
	Engine::Math::Vector2 uv;


	//	Math::Vector4 texcoord;
};

struct VertexS
{
	static constexpr int Format = 3;
	Math::Vector3 position;
	Math::Vector3 normal;
	Math::Vector3 tangent;
	Math::Vector4 color;
	Math::Vector2 uv;
};

struct VertexPNT
{
	static constexpr int Format = 4;
	Math::Vector3 position;
	Math::Vector3 normal;
	Math::Vector2 uv;
};

} // namespace Graphics
} // namespace Engine

#endif 