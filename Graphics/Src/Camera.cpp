#include "Precompiled.h"
#include "Camera.h"
#include "GraphicsSystem.h"

using namespace Engine;
using namespace Engine::Graphics;

Camera::Camera()
	: mPosition{ 0.0f,0.0f,0.0f }
	, mDirection{ 0.0f,0.0f,1.0f }
	, mFOV{ 60.0f * Math::kDegToRad }
	, mNearPlane{ 0.01f }
	, mFarPlane{ 10000.0f }

{}


void Camera::SetPosition(const Math::Vector3& position)
{
	mPosition = position;
}
void Camera::SetDirection(const Math::Vector3& direction)
{
	mDirection = Math::Normalize(direction);
}
void Camera::SetLookAt(const Math::Vector3& target) // vector subtraction
{
	mDirection = Normalize(target - mPosition);
}

void Camera::Walk(float distance) // changing position  compute a new vector, add some multiple of your direction
{
	mPosition += mDirection * distance;
}
void Camera::Strafe(float distance) //cross product
{
	Math::Vector3 right = Math::Cross(Math::Vector3::YAxis(), mDirection);
	mPosition += right * distance;
}

void Camera::Rise(float distance) // update position by the y axis
{
	mPosition += Math::Vector3::YAxis() * distance;
}
void Camera::Fall(float distance) // update position by the y axis
{
	mPosition -= Math::Vector3::YAxis() * distance;
}

void Camera::Yaw(float radian) // do rotation
{
	Math::Matrix matRotate = Math::Matrix::RotationY(radian);
	mDirection = Math::TransformNormal(mDirection, matRotate);
}
void Camera::Pitch(float radian)
{
	const Math::Vector3 right = Normalize(Cross(Math::Vector3::YAxis(), mDirection));
	const Math::Matrix matRotate = Math::Matrix::RotationAxis(right, radian);
	const Math::Vector3 newLook = TransformNormal(mDirection, matRotate);
	const float dot = Dot(newLook, Math::Vector3::YAxis());

	// Avoid looking straight up or down
	if (Math::Abs(dot) < 0.995f)
	{
		mDirection = newLook;
	}
}

void Camera::SetFOV(float fov)
{
	const float kMinFOV = 10.0f * Math::kDegToRad;
	const float kMaxFOV = 150.0f * Math::kDegToRad;
	mFOV = Math::Clamp(fov, kMinFOV, kMaxFOV);
}
void Camera::SetNearPlane(float nearPlane)
{
	mNearPlane = nearPlane;
}
void Camera::SetFarPlane(float farPlane)
{
	mFarPlane = farPlane;
}

Math::Matrix Camera::GetViewMatrix() const
{
	const Math::Vector3 l = mDirection;
	const Math::Vector3 r = Math::Normalize(Math::Cross(Math::Vector3::YAxis(), l));
	const Math::Vector3 u = Math::Normalize(Math::Cross(l, r));
	const float dx = -Math::Dot(r, mPosition); /*Dot*/
	const float dy = -Math::Dot(u, mPosition);
	const float dz = -Math::Dot(l, mPosition);

	return Math::Matrix
	{
		r.x,u.x,l.x,0.0f,
		r.y, u.y, l.y, 0.0f,
		r.z, u.z, l.z, 0.0f,
		dx, dy, dz, 1.0f
	};
}

Math::Matrix Camera::GetPerspectiveMatrix(float aspectRatio) const
{


	const float aspect = (aspectRatio == 0.0f)
		? GraphicsSystem::Get()->GetAspectRatio()
		: aspectRatio;

	//normalize the widrh and height
	const float h = 1 / tan(mFOV * 0.5f);
	const float w = h / aspect;
	const float zf = mFarPlane;
	const float zn = mNearPlane;
	const float d = zf / (zf - zn);

	return Math::Matrix
	(
		w, 0.0f, 0.0f, 0.0f,
		0.0f, h, 0.0f, 0.0f,
		0.0f, 0.0f, d, 1.0f,
		0.0f, 0.0f, -zn * d, 0.0f
	);
}


Math::Matrix Camera::GetProjectionMatrix(uint32_t screenWidth, uint32_t screenHeight) const
{
	const float aspect = (float)screenWidth / (float)screenHeight;
	const float h = 1 / tan(mFOV * 0.5f);
	const float w = h / aspect;
	const float f = mFarPlane;
	const float n = mNearPlane;
	const float d = f / (f - n);

	return Math::Matrix
	(
		w, 0.0f, 0.0f, 0.0f,
		0.0f, h, 0.0f, 0.0f,
		0.0f, 0.0f, d, 1.0f,
		0.0f, 0.0f, -n * d, 0.0f
	);
}

Math::Matrix Camera::GetWorldMatrix() const
{
	Math::Vector3 forward = Math::Normalize(mDirection);
	Math::Vector3 right = Math::Normalize(Math::Cross(Math::Vector3::YAxis(), forward));
	Math::Vector3 up = Math::Normalize(Math::Cross(forward, right));
	return Math::Matrix(
		right.x, right.y, right.z, 0.0f,
		up.x, up.y, up.z, 0.0f,
		forward.x, forward.y, forward.z, 0.0f,
		mPosition.x, mPosition.y, mPosition.z, 1.0f);
}