#include "Precompiled.h"
#include "ConstantBuffer.h"
#include "GraphicsSystem.h"

using namespace Engine;
using namespace Engine::Graphics;


void ConstantBuffer::Initialize()
{
	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = sizeof(Math::Matrix) * 3;
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	Graphics::GraphicsSystem::Get()->GetDevice()->CreateBuffer(&bufferDesc, nullptr, &mConstantBuffer);
}

void ConstantBuffer::Initialize(uint32_t bufferSize, const void* initData)
{
	D3D11_BUFFER_DESC desc;
	ZeroMemory(&desc, sizeof desc);
	desc.ByteWidth = bufferSize;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	D3D11_SUBRESOURCE_DATA subresourceData;
	D3D11_SUBRESOURCE_DATA* subresourceDataPtr = nullptr;
	if (initData != nullptr)
	{
		ZeroMemory(&subresourceData, sizeof(subresourceData));
		subresourceData.pSysMem = initData;
		subresourceDataPtr = &subresourceData;
	}

	GraphicsSystem::Get()->GetDevice()->CreateBuffer(&desc, subresourceDataPtr, &mConstantBuffer);
}


void ConstantBuffer::Terminate()
{
	SafeRelease(mConstantBuffer);
}

void ConstantBuffer::Bind(float x, float y, float z, float rx, float ry)
{
	Math::Matrix translation;
	translation = Math::Matrix::Translation(x, y, x);

	Math::Matrix world, view, projection;
	world = Math::Matrix::RotationY(ry)*Math::Matrix::RotationX(rx)*translation; //Math::Matrix::Transform

	view = Math::Matrix    //view = mCamera.GetViewMatrix()
	(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 15.0f, 1.0f // Set camera at (0,0,-3) looking at the original
	);

	projection = Math::Matrix // projection = mCamera.GetProjectionMatrix()
	(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 0.0f, -1.0f, 1.0f
	);

	auto context = Graphics::GraphicsSystem::Get()->GetContext();

	Math::Matrix matrices[3];
	matrices[0] = Math::Transpose(world);
	matrices[1] = Math::Transpose(view);
	matrices[2] = Math::Transpose(projection);
	context->UpdateSubresource(mConstantBuffer, 0, nullptr, matrices, 0, 0);
	context->VSSetConstantBuffers(0, 1, &mConstantBuffer);

}

void ConstantBuffer::BindTest()
{
	Math::Matrix translation;
	translation = Math::Matrix::Translation(0, 0, 0);

	Math::Matrix world, view, projection;
	world = Math::Matrix::RotationY(rotationY)*Math::Matrix::RotationX(rotationX)*translation; //Math::Matrix::Transform

	view = Math::Matrix    //view = mCamera.GetViewMatrix()
	(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 15.0f, 1.0f // Set camera at (0,0,-3) looking at the original
	);

	projection = Math::Matrix // projection = mCamera.GetProjectionMatrix()
	(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 0.0f, -1.0f, 1.0f
	);

	auto context = Graphics::GraphicsSystem::Get()->GetContext();

	Math::Matrix matrices[3];
	matrices[0] = Math::Transpose(world);
	matrices[1] = Math::Transpose(view);
	matrices[2] = Math::Transpose(projection);
	context->UpdateSubresource(mConstantBuffer, 0, nullptr, matrices, 0, 0);
	context->VSSetConstantBuffers(0, 1, &mConstantBuffer);
	Control();
}

void ConstantBuffer::Set(const void* data)
{
	GraphicsSystem::Get()->GetContext()->UpdateSubresource(mConstantBuffer, 0, nullptr, data, 0, 0);
}

void ConstantBuffer::BindVS(uint32_t slot)
{
	GraphicsSystem::Get()->GetContext()->VSSetConstantBuffers(slot, 1, &mConstantBuffer);
}

void ConstantBuffer::BindPS(uint32_t slot)
{
	GraphicsSystem::Get()->GetContext()->PSSetConstantBuffers(slot, 1, &mConstantBuffer);
}




















void ConstantBuffer::Control()
{
	if (GetAsyncKeyState('A') & 0x8000)
	{
		rotationY = rotationY + 0.1;
	}

	if (GetAsyncKeyState('D') & 0x8000)
	{
		rotationY = rotationY - 0.1;
	}

	if (GetAsyncKeyState('W') & 0x8000)
	{
		rotationX = rotationX + 0.1;
	}

	if (GetAsyncKeyState('S') & 0x8000)
	{
		rotationX = rotationX - 0.1;
	}
}