#include "Precompiled.h"
#include "MeshBuffer.h"
#include "GraphicsSystem.h"

using namespace Engine;
using namespace Engine::Graphics;

void MeshBuffer::Initialize(const void* sVertices, uint32_t vertexPC, uint32_t verticesSize, uint32_t indicesSize, const uint32_t* sIndices)
{
	mVertexSize =  vertexPC;
	mVertexCount = verticesSize;
	mIndexCount = indicesSize;
	
	
	// Fill our buffer description so D3D knows how to create the vertex buffer
	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.ByteWidth = vertexPC * verticesSize;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = 0;
	bufferDesc.MiscFlags = 0;


	// Initialize our vertex buffer by feeding it our triangle vertices
	D3D11_SUBRESOURCE_DATA initData = {};
	initData.pSysMem = sVertices;

	//Create the vertex buffer using the D3D device
	Graphics::GraphicsSystem::Get()->GetDevice()->CreateBuffer(&bufferDesc, &initData, &mVertexBuffer);

	bufferDesc.ByteWidth = static_cast<UINT>(sizeof(uint32_t) * indicesSize);
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	initData.pSysMem = sIndices;

	Graphics::GraphicsSystem::Get()->GetDevice()->CreateBuffer(&bufferDesc, &initData, &mIndexBuffer);
}

void MeshBuffer::Terminate()
{
	SafeRelease(mVertexBuffer);
	SafeRelease(mIndexBuffer);
}

void MeshBuffer::Draw()
{
	ID3D11DeviceContext* context = Graphics::GraphicsSystem::Get()->GetContext();

	// Set vertex buffer
	UINT stride = mVertexSize;
	UINT offset = 0;
	context->IASetVertexBuffers(0, 1, &mVertexBuffer, &stride, &offset);

	// Set primitive topology
	context->IASetPrimitiveTopology(mTopology);

	if (mIndexBuffer == nullptr)
	{
		// Draw MeshBuffer (if you use vertex buffer only)
		context->Draw(mVertexCount, 0);
	}
	else
	{
		// Set index buffer
		context->IASetIndexBuffer(mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		// Draw MeshBuffer (if you are using both vertex buffer + index buffer)
		context->DrawIndexed(mIndexCount, 0, 0);
	}
}

void MeshBuffer::Initialize(const MeshPC& mesh)
{
	Initialize(mesh.GetVertices(), sizeof(VertexPC),mesh.GetVertexSize(),  mesh.GetIndexCount(), mesh.GetIndices());
}
void MeshBuffer::Initialize(const MeshS& mesh)
{
	Initialize(mesh.GetVertices(), mesh.GetVertexCountS(), mesh.GetVertexSize(), mesh.GetIndexCount(), mesh.GetIndices());
}

void MeshBuffer::Initialize(const MeshPNT& mesh)
{
	Initialize(mesh.GetVertices(), mesh.GetVertexCountPNT(), mesh.GetVertexSize(), mesh.GetIndexCount(), mesh.GetIndices());
}


void MeshBuffer::Initialize(const MeshPT& mesh)
{
	Initialize(mesh.GetVertices(), sizeof(VertexPT), mesh.GetVertexSize(), mesh.GetIndexCount(), mesh.GetIndices());
}


void MeshBuffer::Draw(uint32_t vertexPC, uint32_t indicesSize)
{
	ID3D11DeviceContext* context = Graphics::GraphicsSystem::Get()->GetContext();
	// Connect our vertext buffer
	UINT stride = vertexPC;
	UINT offset = 0;
	context->IASetVertexBuffers(0, 1, &mVertexBuffer, &stride, &offset);
	context->IASetIndexBuffer(mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set primitive topology
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Draw mesh
	context->DrawIndexed(indicesSize, 0, 0);
}