#include "Precompiled.h"
#include "GraphicsSystem.h"
#include "MeshBuilder.h"


using namespace Engine;
using namespace Engine::Graphics;


void MeshBuilder::GenerateCubePC(MeshPC& mesh)
{
 
	const Graphics::VertexPC sVertices[] =
	{
	{ { -1.0f, -1.0, -1.0f } ,{ 1.0f, 0.0f, 0.0f, 0.0f } }, //FRONT
	{ { -1.0f,  1.0f, -1.0f } ,{ 1.0f, 0.0f, 0.0f, 0.0f } },
	{ { 1.0f,  -1.0f, -1.0f } ,{ 1.0f, 0.0f, 0.0f, 0.0f } },

	{ { -1.0f, 1.0, -1.0f } ,{ 1.0f, 0.0f, 0.0f, 0.0f } }, //FRONT
	{ { 1.0f,  1.0f, -1.0f } ,{ 1.0f, 0.0f, 0.0f, 0.0f } },
	{ { 1.0f,  -1.0f, -1.0f } ,{ 1.0f, 0.0f, 0.0f, 0.0f } },




	{ { -1.0f, -1.0, 1.0f } ,{ 0.0f, 1.0f, 0.0f, 0.0f } }, //BACK
	{ { 1.0f,  -1.0f, 1.0f } ,{ 0.0f, 1.0f, 0.0f, 0.0f } },
	{ { -1.0f,  1.0f, 1.0f } ,{ 0.0f, 1.0f, 0.0f, 0.0f } },

	{ { 1.0f,  1.0f, 1.0f } ,{ 0.0f, 1.0f, 0.0f, 0.0f } }, //BACK
	{ { -1.0f, 1.0, 1.0f } ,{ 0.0f, 1.0f, 0.0f, 0.0f } },
	{ { 1.0f,  -1.0f, 1.0f } ,{ 0.0f, 1.0f, 0.0f, 0.0f } },




	{ { -1.0f,  1.0f, -1.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f } },  //LEFT
	{ { -1.0f,  -1.0f, -1.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f } },
	{ { -1.0f,  -1.0f, 1.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f } },

	{ { -1.0f,  1.0f, 1.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f } }, //LEFT
	{ { -1.0f,  1.0f, -1.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f } },
	{ { -1.0f,  -1.0f, 1.0f } ,{ 1.0f, 1.0f, 0.0f, 1.0f } },




	{ { 1.0f,  -1.0f, -1.0f } ,{ 0.0f, 0.0f, 1.0f, 1.0f } }, //RIGHT
	{ { 1.0f,  1.0f, -1.0f } ,{ 0.0f, 0.0f, 1.0f, 1.0f } },
	{ { 1.0f,  -1.0f, 1.0f } ,{ 0.0f, 0.0f, 1.0f, 1.0f } },

	{ { 1.0f,  1.0f, -1.0f } ,{ 0.0f, 0.0f, 1.0f, 1.0f } }, //RIGHT
	{ { 1.0f,  1.0f, 1.0f } ,{ 0.0f, 0.0f, 1.0f, 1.0f } },
	{ { 1.0f,  -1.0f, 1.0f } ,{ 0.0f, 0.0f, 1.0f, 1.0f } },




	{ { 1.0f,  1.0f, -1.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f } }, //TOP
	{ { -1.0f,  1.0f, -1.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f } },
	{ { 1.0f,  1.0f, 1.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f } },

	{ { -1.0f,  1.0f, -1.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f } }, //TOP
	{ { -1.0f,  1.0f, 1.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f } },
	{ { 1.0f,  1.0f, 1.0f } ,{ 1.0f, 0.0f, 1.0f, 1.0f } },




	{ { 1.0f,  -1.0f, -1.0f } ,{ 1.0f, 0.5f, 0.0f, 1.0f } }, //BOTTOM
	{ { 1.0f,  -1.0f, 1.0f } ,{ 1.0f, 0.5f, 0.0f, 1.0f } },
	{ { -1.0f,  -1.0f, -1.0f } ,{ 1.0f, 0.5f, 0.0f, 1.0f } },

	{ { -1.0f,  -1.0f, 1.0f } ,{ 1.0f, 0.5f, 0.0f, 1.0f } }, //BOTTOM
	{ { -1.0f,  -1.0f, -1.0f } ,{ 1.0f, 0.5f, 0.0f, 1.0f } },
	{ { 1.0f,  -1.0f, 1.0f } ,{ 1.0f, 0.5f, 0.0f, 1.0f } },

	};

	const uint32_t sIndices[] =
	{
		0,1,2,
		3,4,5,

		6,7,8,
		9,10,11,

		12,13,14,
		15,16,17,

		18,19,20,
		21,22,23,

		24,25,26,
		27,28,29,

		30,31,32,
		33,34,35
	};


	mesh.Allocate(std::size(sVertices), std::size(sIndices));

	for (uint32_t i = 0; i < std::size(sVertices); i++)
	{
		mesh.GetVertex(i) = sVertices[i];
	}

	for (uint32_t i = 0; i < std::size(sIndices); i++)
	{
		mesh.GetIndex(i) = sIndices[i];
	}
}

void MeshBuilder::GenerateCubePT(MeshPT& mesh)
{

	const Graphics::VertexPT sVertices[] =
	{
		/*0*/ { Math::Vector3(-1.0f, -1.0f, -1.0f), Math::Vector2(0.0f, 1.0f) },
		/*1*/ { Math::Vector3(-1.0f, 1.0f, -1.0f), Math::Vector2(0.0f, 0.0f) },
		/*2*/ { Math::Vector3( 1.0f, 1.0f, -1.0f), Math::Vector2(1.0f, 0.0f) },
		/*3*/{ Math::Vector3(1.0f, -1.0f, -1.0f), Math::Vector2(1.0f, 1.0f) },
		

		/*1*/{ Math::Vector3(-1.0f, 1.0f, -1.0f), Math::Vector2(0.0f, 1.0f) },
		/*4*/{ Math::Vector3(-1.0f, 1.0f, 1.0f), Math::Vector2(0.0f, 0.0f) },
		/*5*/{ Math::Vector3(1.0f, 1.0f, 1.0f), Math::Vector2(1.0f, 0.0f) },
		/*2*/{ Math::Vector3(1.0f, 1.0f, -1.0f), Math::Vector2(1.0f, 1.0f) },

		/*3*/{ Math::Vector3(1.0f, -1.0f, -1.0f), Math::Vector2(0.0f, 1.0f) },
		/*2*/{ Math::Vector3(1.0f, 1.0f, -1.0f), Math::Vector2(0.0f, 0.0f) },
		/*4*/{ Math::Vector3(1.0f, 1.0f, 1.0f), Math::Vector2(1.0f, 0.0f) },
		/*7*/{ Math::Vector3(1.0f, -1.0f, 1.0f), Math::Vector2(1.0f, 1.0f) },

		/*6*/{ Math::Vector3(-1.0f, -1.0f, 1.0f), Math::Vector2(0.0f, 1.0f) },
		/*4*/{ Math::Vector3(-1.0f, 1.0f, 1.0f), Math::Vector2(0.0f, 0.0f) },
		/*1*/{ Math::Vector3(-1.0f, 1.0f, -1.0f), Math::Vector2(1.0f, 0.0f) },
		/*0*/{ Math::Vector3(-1.0f, -1.0f, -1.0f), Math::Vector2(1.0f, 1.0f) },

		/*7*/{ Math::Vector3(1.0f, -1.0f, 1.0f), Math::Vector2(0.0f, 1.0f) },
		/*5*/{ Math::Vector3(1.0f, 1.0f, 1.0f), Math::Vector2(0.0f, 0.0f) },
		/*4*/{ Math::Vector3(-1.0f, 1.0f, 1.0f), Math::Vector2(1.0f, 0.0f) },
		/*6*/{ Math::Vector3(-1.0f, -1.0f, 1.0f), Math::Vector2(1.0f, 1.0f) },
		
		
		/*6*/{ Math::Vector3(-1.0f, -1.0f, 1.0f), Math::Vector2(0.0f, 1.0f) },
		/*0*/{ Math::Vector3(-1.0f, -1.0f, -1.0f), Math::Vector2(0.0f, 0.0f) },
		/*3*/{ Math::Vector3(1.0f, -1.0f, -1.0f), Math::Vector2(1.0f, 0.0f) },
		/*7*/{ Math::Vector3(1.0f, -1.0f, 1.0f), Math::Vector2(1.0f, 1.0f) },
		
		
		

		/*0*/ { Math::Vector3(-1.0f, -1.0f, -1.0f), Math::Vector2(0.0f, 1.0f) },
		/*1*/ { Math::Vector3(-1.0f, 1.0f, -1.0f), Math::Vector2(0.0f, 0.0f) },
		/*2*/ { Math::Vector3(1.0f, 1.0f, -1.0f), Math::Vector2(1.0f, 0.0f) },
		/*3*/ { Math::Vector3(1.0f, -1.0f, -1.0f), Math::Vector2(1.0f, 1.0f) },
		/*4*/ { Math::Vector3(-1.0f, 1.0f, 1.0f), Math::Vector2(0.0f, 0.0f) },
		/*5*/ { Math::Vector3( 1.0f, 1.0f, 1.0f), Math::Vector2(1.0f, 1.0f) },
		/*6*/ { Math::Vector3(-1.0f, -1.0f, 1.0f), Math::Vector2(0.0f, 1.0f) },
		/*7*/ { Math::Vector3(1.0f, -1.0f, 1.0f), Math::Vector2(0.0f, 0.0f) }
	};

	const uint32_t sIndices[] =
	{
		0, 1, 2,
		0, 2, 3,
		4, 5, 6,
		4, 6, 7,
		8,9,10,
		8,10,11,
		12,13,14,
		12,14,15,
		16,17,18,
		16,18,19,
		20,21,22,
		20,22,23
	};

	mesh.Allocate(std::size(sVertices), std::size(sIndices));

	for (uint32_t i = 0; i < std::size(sVertices); i++)
	{
		mesh.GetVertex(i) = sVertices[i];
	}

	for (uint32_t i = 0; i < std::size(sIndices); i++)
	{
		mesh.GetIndex(i) = sIndices[i];
	}


}


void MeshBuilder::GeneratePyramidPT(MeshPT& mesh)
{
	const Graphics::VertexPT sVertices[] =
	{
		{ Math::Vector3(-1.0f, -1.0f, -1.0f), Math::Vector2(0.0f, 1.0f) },
		{ Math::Vector3(0.0f, 1.0f, 0.0f), Math::Vector2(0.0f, 0.0f) },
		{ Math::Vector3(1.0f, -1.0f, -1.0f), Math::Vector2(1.0f, 0.0f) },

		{ Math::Vector3(1.0f, -1.0f, -1.0f), Math::Vector2(0.0f, 1.0f) },
		{ Math::Vector3(0.0f, 1.0f, 0.0f), Math::Vector2(0.0f, 0.0f) },
		{ Math::Vector3(1.0f, -1.0f, 1.0f), Math::Vector2(1.0f, 0.0f) },

		{ Math::Vector3(1.0f, -1.0f, 1.0f), Math::Vector2(0.0f, 1.0f) },
		{ Math::Vector3(0.0f, 1.0f, 0.0f), Math::Vector2(0.0f, 0.0f) },
		{ Math::Vector3(-1.0f, -1.0f, 1.0f), Math::Vector2(1.0f, 0.0f) },

		{ Math::Vector3(-1.0f, -1.0f, 1.0f), Math::Vector2(0.0f, 1.0f) },
		{ Math::Vector3(0.0f, 1.0f, 0.0f), Math::Vector2(0.0f, 0.0f) },
		{ Math::Vector3(-1.0f, -1.0f, -1.0f), Math::Vector2(1.0f, 0.0f) },

		{ Math::Vector3(-1.0f, -1.0f, 1.0f), Math::Vector2(0.0f, 1.0f) },
		{ Math::Vector3(-1.0f, -1.0f, -1.0f), Math::Vector2(0.0f, 0.0f) },
		{ Math::Vector3(1.0f, -1.0f, -1.0f), Math::Vector2(1.0f, 0.0f) },
		{ Math::Vector3(1.0f, -1.0f, 1.0f), Math::Vector2(1.0f, 1.0f) },
	};

	const uint32_t sIndices[] =
	{
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ,
		12 ,13, 14,
		12, 14, 15
	};

	mesh.Allocate(std::size(sVertices), std::size(sIndices));

	for (uint32_t i = 0; i < std::size(sVertices); i++)
	{
		mesh.GetVertex(i) = sVertices[i];
	}

	for (uint32_t i = 0; i < std::size(sIndices); i++)
	{
		mesh.GetIndex(i) = sIndices[i];
	}

}


void MeshBuilder::GenerateSphereS(MeshS& mesh, uint32_t rings, uint32_t slices)
{
	const uint32_t kNumVertices = (slices + 1) * rings;
	const uint32_t kNumIndices = slices * (rings - 1) * 6;
	const float kSliceStep = Math::kTwoPi / slices;
	const float kRingStep = Math::kPi / (rings - 1);

	// Allocate memory
	mesh.Allocate(kNumVertices, kNumIndices);

	// Fill vertex data
	float uStep = 1.0f / slices;
	float vStep = 1.0f / rings;
	uint32_t index = 0;
	for (uint32_t j = 0; j < rings; ++j)
	{
		const float phi = j * kRingStep;
		for (uint32_t i = 0; i <= slices; ++i)
		{
			const float theta = i * kSliceStep;
			const float y = cos(phi);
			const float r = sqrt(1.0f - (y * y));
			const float s = sin(theta);
			const float c = cos(theta);
			const float x = r * c;
			const float z = r * s;

			mesh.GetVertex(index).position = Math::Vector3(x, y, z);
			mesh.GetVertex(index).normal = Math::Vector3(x, y, z);
			mesh.GetVertex(index).tangent = Math::Vector3(-s, 0.0f, c);
			mesh.GetVertex(index).color = Math::Vector4(0.0f, 0.0f, 0.0f, 0.0f);
			mesh.GetVertex(index).uv = Math::Vector2(i * uStep, j * vStep);

			++index;
		}
	}

	// Fill index data
	index = 0;
	for (uint32_t j = 0; j < rings - 1; ++j)
	{
		for (uint32_t i = 0; i < slices; ++i)
		{
			const uint32_t a = i % (slices + 1);
			const uint32_t b = (i + 1) % (slices + 1);
			const uint32_t c = j * (slices + 1);
			const uint32_t d = (j + 1) * (slices + 1);

			// draw
			mesh.GetIndex(index++) = a + c;
			mesh.GetIndex(index++) = b + c;
			mesh.GetIndex(index++) = a + d;

			mesh.GetIndex(index++) = b + c;
			mesh.GetIndex(index++) = b + d;
			mesh.GetIndex(index++) = a + d;
		}
	}
}




void MeshBuilder::GenerateSpherePNT(MeshPNT& mesh, uint32_t rings, uint32_t slices)
{
	const uint32_t kNumVertices = (slices + 1) * rings;
	const uint32_t kNumIndices = slices * (rings - 1) * 6;
	const float kSliceStep = Math::kTwoPi / slices;
	const float kRingStep = Math::kPi / (rings - 1);

	// Allocate memory
	mesh.Allocate(kNumVertices, kNumIndices);

	// Fill vertex data
	float uStep = 1.0f / slices;
	float vStep = 1.0f / rings;
	uint32_t index = 0;
	for (uint32_t j = 0; j < rings; ++j)
	{
		const float phi = j * kRingStep;
		for (uint32_t i = 0; i <= slices; ++i)
		{
			const float theta = i * kSliceStep;
			const float y = cos(phi);
			const float r = sqrt(1.0f - (y * y));
			const float s = sin(theta);
			const float c = cos(theta);
			const float x = r * c;
			const float z = r * s;

			mesh.GetVertex(index).position = Math::Vector3(x, y, z);
			mesh.GetVertex(index).normal = Math::Vector3(x, y, z);
			mesh.GetVertex(index).uv = Math::Vector2(i * uStep, j * vStep);

			++index;
		}
	}

	// Fill index data
	index = 0;
	for (uint32_t j = 0; j < rings - 1; ++j)
	{
		for (uint32_t i = 0; i < slices; ++i)
		{
			const uint32_t a = i % (slices + 1);
			const uint32_t b = (i + 1) % (slices + 1);
			const uint32_t c = j * (slices + 1);
			const uint32_t d = (j + 1) * (slices + 1);
			mesh.GetIndex(index++) = a + c;
			mesh.GetIndex(index++) = b + c;
			mesh.GetIndex(index++) = a + d;

			mesh.GetIndex(index++) = b + c;
			mesh.GetIndex(index++) = b + d;
			mesh.GetIndex(index++) = a + d;
		}
	}
}





void MeshBuilder::GenerateSkydomeS(MeshS& mesh, uint32_t rings, uint32_t slices)
{
	const uint32_t kNumVertices = (slices + 1) * rings;
	const uint32_t kNumIndices = slices * (rings - 1) * 6;
	const float kSliceStep = Math::kTwoPi / slices;
	const float kRingStep = Math::kPi / (rings - 1);

	// Allocate memory
	mesh.Allocate(kNumVertices, kNumIndices);

	// Fill vertex data
	float uStep = 1.0f / slices;
	float vStep = 1.0f / rings;
	uint32_t index = 0;
	for (uint32_t j = 0; j < rings; ++j)
	{
		const float phi = j * kRingStep;
		for (uint32_t i = 0; i <= slices; ++i)
		{
			const float theta = i * kSliceStep;
			const float y = cos(phi);
			const float r = sqrt(1.0f - (y * y));
			const float s = sin(theta);
			const float c = cos(theta);
			const float x = r * c;
			const float z = r * s;

			Math::Vector3 p(-x, -y, -z);
			Math::Vector3 n(1, y, z);
			Math::Vector3 t(-s, 0.0f, c);
			Math::Vector2 uv(i * uStep, j * vStep);

			mesh.GetVertex(index).position = p;
			mesh.GetVertex(index).normal = n;
			mesh.GetVertex(index).tangent = t;
			mesh.GetVertex(index).color = Math::Vector4(1.0f, 0.0f, 1.0f, 0.0f);
			mesh.GetVertex(index).uv = uv;

			++index;
		}
	}

	for (uint32_t j = 0; j < rings - 1; ++j)
	{
		for (uint32_t i = 0; i < slices; ++i)
		{
			const uint32_t a = i % (slices + 1);
			const uint32_t b = (i + 1) % (slices + 1);
			const uint32_t c = j * (slices + 1);
			const uint32_t d = (j + 1) * (slices + 1);

			mesh.AddIndices(a + c);
			mesh.AddIndices(b + c);
			mesh.AddIndices(a + d);
			mesh.AddIndices(b + c);
			mesh.AddIndices(b + d);
			mesh.AddIndices(a + d);
		}
	}
}

