#include "Precompiled.h"
#include "PixelShader.h"
#include "GraphicsSystem.h"

using namespace Engine;
using namespace Engine::Graphics;

void PixelShader::Initialize(const wchar_t* filename)
{
	DWORD shaderFlags = D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_DEBUG;
	ID3DBlob* shaderBlob = nullptr;
	ID3DBlob* errorBlob = nullptr;

	// Compile the pixel shader code
	D3DCompileFromFile(
		filename,
		nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"PS", "ps_4_0",
		shaderFlags, 0, &shaderBlob, &errorBlob);

	// Create pixel shader
	Graphics::GraphicsSystem::Get()->GetDevice()->CreatePixelShader(
		shaderBlob->GetBufferPointer(),
		shaderBlob->GetBufferSize(),
		nullptr,
		&mPixelShader);


}

void PixelShader::Bind()
{
	ID3D11DeviceContext* context = Graphics::GraphicsSystem::Get()->GetContext();
	context->PSSetShader(mPixelShader, nullptr, 0);
}

void PixelShader::Terminate()
{
	SafeRelease(mPixelShader);
}