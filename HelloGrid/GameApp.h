#ifndef INCLUDED_GAMEAPP_H
#define INCLUDED_GAMEAPP_H

#include <Core/Inc/Core.h>
#include <Graphics/Inc/Graphics.h>
#include <Input/Inc/Input.h>
#include <Math/Inc/EngineMath.h>

class GameApp : public Engine::Core::Application
{
public:
	GameApp();
	~GameApp() override;

private:
	void OnInitialize() override;
	void OnTerminate() override;
	void OnUpdate() override;

	Engine::Core::Window mWindow;
	Engine::Input::InputSystem mInput;
	Engine::Graphics::ConstantBuffer mConstantBuffer;
	Engine::Graphics::Camera mCamera;

	Engine::Graphics::MeshS mSphere;
	Engine::Graphics::MeshBuilder mSphereBuilder;
	Engine::Graphics::MeshBuffer mSphereBuffer;

	Engine::Graphics::MeshPC mCube;
	Engine::Graphics::MeshBuilder mCubeBuilder;

	Engine::Graphics::MeshPT mCubeTwo;
	Engine::Graphics::MeshBuilder mCubeBuilderTwo;

	Engine::Graphics::Texture mTexture;
	Engine::Graphics::Sampler mSampler;

	Engine::Graphics::Texture mTextureSphere;
	Engine::Graphics::Sampler mSamplerSphere;

	Engine::Graphics::VertexShader mVertexShader;
	Engine::Graphics::PixelShader mPixelShader;
	Engine::Graphics::MeshBuffer mMeshBuffer;

	Engine::Graphics::VertexShader mVertexShaderSphere;
	Engine::Graphics::PixelShader mPixelShaderSphere;

	Engine::Graphics::PixelShader mTextureShader;
	Engine::Graphics::MeshBuffer mTextureBuffer;

	Engine::Graphics::PixelShader mTextureShaderCube;
	Engine::Graphics::MeshBuffer mTextureBufferCube;

	Engine::Graphics::MeshPT Pyramid;
	Engine::Graphics::MeshBuilder mPyramidBuilder;
	Engine::Graphics::MeshBuffer mPyramidBuffer;
	Engine::Graphics::VertexShader mPyramidVertexShader;
	Engine::Graphics::PixelShader mPyramidPixelShader;
	Engine::Graphics::Texture mPyramidTexture;

	Engine::Graphics::SimpleDraw mDraw;







	uint32_t maxVertices = 10000;
	float rotationY = 0.0f;
	float rotationX = 0.0f;

	bool Switch = false;

	float FOV = 1.0f;

	

};

#endif //#ifndef INCLUDED_GAMEAPP_H