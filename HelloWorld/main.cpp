#include <Math/Inc/EngineMath.h>

int main(int argc, char* argv[])
{
	//Engine::Math::Vector2 u{ 1.0f, 0.0f };
	//Engine::Math::Vector2 v{ 0.0f, 1.0f };

	Engine::Math::Vector2 u{ -6.0f, 8.0f };
	Engine::Math::Vector2 v{ 5.0f, 12.0f };

	printf("u = 1, 0 \n");
	printf("v = 0, 1 \n");

	Engine::Math::Vector2 w = u + v;
	printf("w = u + v \n");
	printf("w = %f, %f \n", w.x, w.y);


	printf("x = %.2f, %.2f \n", u.x, u.y);
	printf("y = %.2f, %.2f \n", v.x, v.y);
	float a = Engine::Math::Vector2::Dot(u, v);
	printf("Dot product = %f \n", a);

	system("pause");
	return 0;
}


/*

IDE -  Integrated Development Environment
- Master program with a lot of tools
- TextEditor
- Compiler
- Linker
- Debugger
- Static Analyzer
- Unit Tester


static === comoile time > exe
dynamic === run time > process

- Precondition: n needs to be a positive integer (what needs to be true)
- Postcondition: Fib computes the correct result

#include <cassert>
int Fib(int n )
{
	assert(n > 0);
	if (n == 1 || n == 2)
		return 1;
	return Fib(n - 1) + Fib(n - 2);
}

void main()
{
	int i;						static allocation
	float f[10];				static array
	int* myStuff = new int[i];	dynamic allocation
}

https://www.mathportal.org/calculators/matrices-calculators/vector-calculator.php
*/