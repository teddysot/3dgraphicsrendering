#ifndef INCLUDED_MATH_H
#define INCLUDED_MATH_H

#include "Common.h"

#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"
#include "Matrix.h"
#include "Quaternion.h"

namespace Engine {
namespace Math {

extern const float kPi;
extern const float kTwoPi;
extern const float kPiByTwo;
extern const float kRootTwo;
extern const float kRootThree;
extern const float kDegToRad;
extern const float kRadToDeg;

float DotProduct(const Vector2& u, const Vector2& v);

inline float Abs(float value) { return (value >= 0.0f) ? value : -value; }
inline float Sign(float value) { return (value >= 0.0f) ? 1.0f : -1.0f; }
inline float Sqr(float value) { return value * value; }
inline float Sqrt(float value) { return sqrtf(value); }

inline float MagnitudeSqr(const Vector2& v) { return (v.x * v.x) + (v.y * v.y); }
inline float MagnitudeSqr(const Vector3& v) { return (v.x * v.x) + (v.y * v.y) + (v.z * v.z); }
inline float Magnitude(const Vector2& v) { return Sqrt(MagnitudeSqr(v)); }
inline float Magnitude(const Vector3& v) { return Sqrt(MagnitudeSqr(v)); }
inline float MagnitudeXZSqr(const Vector3& v) { return (v.x * v.x) + (v.z * v.z); }
inline float MagnitudeXZ(const Vector3& v) { return Sqrt(MagnitudeXZSqr(v)); }

inline Vector2 Normalize(const Vector2& v) { return v / Magnitude(v); }
inline Vector3 Normalize(const Vector3& v) { return v / Magnitude(v); }

inline Vector3 Cross(const Vector3& a, const Vector3& b) { return Vector3((a.y * b.z) - (a.z * b.y), (a.z * b.x) - (a.x * b.z), (a.x * b.y) - (a.y * b.x)); }

inline float Dot(const Vector2& a, const Vector2& b) { return (a.x * b.x) + (a.y * b.y); }
inline float Dot(const Vector3& a, const Vector3& b) { return (a.x * b.x) + (a.y * b.y) + (a.z * b.z); }

template <typename T> inline T Min(T a, T b) { return (a > b) ? b : a; }
template <typename T> inline T Max(T a, T b) { return (a < b) ? b : a; }
template <typename T> inline T Clamp(T value, T min, T max) { return Max(min, Min(max, value)); }

inline Vector3 TransformNormal(const Vector3& v, const Matrix& m)
{
	return Vector3
	(
		v.x * m._11 + v.y * m._21 + v.z * m._31,
		v.x * m._12 + v.y * m._22 + v.z * m._32,
		v.x * m._13 + v.y * m._23 + v.z * m._33
	);
}


} // namespace Math
} // namespace Engine

#endif // #ifndef INCLUDED_MATH_H