#ifndef INCLUDED_MATH_QUATERNION_H
#define INCLUDED_MATH_QUATERNION_H
namespace Engine {
	namespace Math {

		struct Quaternion
		{
			float x, y, z, w;

			Quaternion() : x(0.0f), y(0.0f), z(0.0f), w(1.0f) {}
			Quaternion(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {}

			static Quaternion Zero() { return Quaternion(0.0f, 0.0f, 0.0f, 0.0f); }
			static Quaternion Identity() { return Quaternion(0.0f, 0.0f, 0.0f, 1.0f); }

			Quaternion operator+(const Quaternion& rhs) const { return Quaternion(x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w); }
			Quaternion operator*(float s) const { return Quaternion(x * s, y * s, z * s, w * s); }
			Quaternion operator/(float s) const { return Quaternion(x / s, y / s, z / s, w / s); }
			static Quaternion RotationAxis(const Vector3& axis, float rad);
 
		};

	} // namespace Math
}

#endif // #ifndef INCLUDED_MATH_QUATERNION_H