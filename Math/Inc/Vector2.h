#ifndef INCLUDED_MATH_VECTOR2_H
#define INCLUDED_MATH_VECTOR2_H

namespace Engine {
namespace Math {

struct Vector2
{
 
	float x, y;

	Vector2() : x(0.0f), y(0.0f) {}
	Vector2(float inX, float inY) : x(inX), y(inY) {}

	static Vector2 Zero() { return Vector2(); }
	static Vector2 One() { return Vector2(1.0f, 1.0f); }
	static Vector2 XAxis() { return Vector2(1.0f, 0.0f); }
	static Vector2 YAxis() { return Vector2(0.0f, 1.0f); }

	Vector2 operator-() const { return Vector2(-x, -y); }
	Vector2 operator+(const Vector2& rhs) const { return Vector2(x + rhs.x, y + rhs.y); }
	Vector2 operator-(const Vector2& rhs) const { return Vector2(x - rhs.x, y - rhs.y); }
	Vector2 operator*(float s) const { return Vector2(x * s, y * s); }
	Vector2 operator/(float s) const { return Vector2(x / s, y / s); }

	Vector2& operator+=(const Vector2& rhs) { x += rhs.x; y += rhs.y; return *this; }
	Vector2& operator-=(const Vector2& rhs) { x -= rhs.x; y -= rhs.y; return *this; }
	Vector2& operator*=(float s) { x *= s; y *= s; return *this; }
	Vector2& operator/=(float s) { x /= s; y /= s; return *this; }

	//Vector2::Vector2(float X, float Y)
	//	:x(X),y(Y)
	//{
	//}

	//Vector2 operator+(const Vector2& v)
	//{
	//	return { x + v.x, y +v.y };
	//}

	//Vector2 operator-(const Vector2& v)
	//{
	//	return { x - v.x, y - v.y };
	//}

	//Vector2 operator*(const Vector2& v)
	//{
	//	return { x * v.x, y * v.y };
	//}

	//rotation
	//translation
	//ndc normalize device coordinate

	//Vector2 operator/(float v)
	//{
	//	return { x / v, y / v };
	//}

	static float Dot(const Vector2& u, const Vector2& v)
	{
		float d1 = u.x * v.x;
		float d2 = u.y * v.y;
		return d1 + d2;
	}

	//static float Magnitude(const Vector2& v)
	//{
	//	return sqrt(((v.x)*(v.x))+((v.y)*(v.y)));
	//}

	//static Vector2 Add(const Vector2& u, const Vector2& v)
	//{
	//	return Vector2(u.x + v.x, u.y + v.y);
	//}

	//static Vector2 Subtract(const Vector2& u, const Vector2& v)
	//{
	//	return Vector2(u.x - v.x, u.y - v.y);
	//}

	//static Vector2 Multiply(const Vector2& u, const Vector2& v)
	//{
	//	return Vector2(u.x * v.x, u.y * v.y);
	//}

	//static Vector2 Divide(const Vector2& u, const Vector2& v)
	//{
	//	return Vector2(u.x / v.x, u.y / v.y);
	//}

};

} // namespace Math
} // namespace Engine

#endif // #ifndef INCLUDED_MATH_VECTOR2_H