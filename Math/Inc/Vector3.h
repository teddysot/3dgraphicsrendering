#ifndef INCLUDED_MATH_VECTOR3_H
#define INCLUDED_MATH_VECTOR3_H

namespace Engine {
	namespace Math {

		struct Vector3
		{
			float x, y, z;

			Vector3() : x(0.0f), y(0.0f), z(0.0f) {}
			Vector3(float inX, float inY, float inZ) : x(inX), y(inY), z(inZ) {}

			static Vector3 Zero() { return Vector3(); }
			static Vector3 One() { return Vector3(1.0f, 1.0f, 1.0f); }
			static Vector3 XAxis() { return Vector3(1.0f, 0.0f, 0.0f); }
			static Vector3 YAxis() { return Vector3(0.0f, 1.0f, 0.0f); }
			static Vector3 ZAxis() { return Vector3(0.0f, 0.0f, 1.0f); }

			Vector3 operator-() const { return Vector3(-x, -y, -z); }
			Vector3 operator+(const Vector3& rhs) const { return Vector3(x + rhs.x, y + rhs.y, z + rhs.z); }
			Vector3 operator-(const Vector3& rhs) const { return Vector3(x - rhs.x, y - rhs.y, z - rhs.z); }
			Vector3 operator*(float s) const { return Vector3(x * s, y * s, z * s); }
			Vector3 operator/(float s) const { return Vector3(x / s, y / s, z / s); }

			Vector3& operator+=(const Vector3& rhs) { x += rhs.x; y += rhs.y; z += rhs.z; return *this; }
			Vector3& operator-=(const Vector3& rhs) { x -= rhs.x; y -= rhs.y; z -= rhs.z; return *this; }
			Vector3& operator*=(float s) { x *= s; y *= s; z *= s; return *this; }
			Vector3& operator/=(float s) { x /= s; y /= s; z /= s; return *this; }
		};



	} // namespace Math
} // namespace Engine

#endif // #ifndef INCLUDED_MATH_VECTOR3_H