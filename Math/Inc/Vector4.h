#ifndef INCLUDED_MATH_VECTOR4_H
#define INCLUDED_MATH_VECTOR4_H

namespace Engine {
	namespace Math {

		struct Vector4
		{
			union
			{
				struct { float x, y, z, w; };
				struct { float r, g, b, a; };
			};

			Vector4() : x(0.0f), y(0.0f), z(0.0f), w(0.0f) {}
			Vector4(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {}
			Vector4(const Vector3& v, float w) : x(v.x), y(v.y), z(v.z), w(w) {}
		};

	} // namespace Math
} // namespace Engine

#endif // #ifndef INCLUDED_MATH_VECTOR3_H