#include "Precompiled.h"
#include "EngineMath.h"

using namespace Engine;
using namespace Math;

const float Math::kPi = 3.14159265358979f;
const float Math::kTwoPi = 6.28318530717958f;
const float Math::kPiByTwo = 1.57079632679489f;
const float Math::kRootTwo = 1.41421356237309f;
const float Math::kRootThree = 1.73205080756887f;
const float Math::kDegToRad = kPi / 180.0f;
const float Math::kRadToDeg = 180.0f / kPi;

float Math::DotProduct(const Vector2& u, const Vector2& v)
{
	/*
	 u * v = ||u||*||v|| * cos(theta)
	 u * v = (u.x*u.y) + (u.y*v.y)
	|| u || *|| v || * cos(theta) = (u.x*u.y) + (u.y*v.y)
	cos(theta) = (|| u || *|| v |)| / ((u.x*u.y) + (u.y*v.y))
	(theta)cos^-1 = (|| u || *|| v |)| / ((u.x*u.y) + (u.y*v.y))

	// Scalar product, inner product
	// find the angular relationship between 2 angles

	*/
	float d1 = u.x * v.x;
	float d2 = u.y * v.y;

	return d1 + d2;
}

Quaternion Quaternion::RotationAxis(const Vector3& axis, float rad)
{
	const float c = cos(rad * 0.5f);
	const float s = sin(rad * 0.5f);
	const Math::Vector3 a = Math::Normalize(axis);
	return Quaternion(a.x * s, a.y * s, a.z * s, c);


}



Matrix Matrix::RotationAxis(const Vector3& axis, float rad)
{
	const Vector3 u = Normalize(axis);
	const float x = u.x;
	const float y = u.y;
	const float z = u.z;
	const float s = sin(rad);
	const float c = cos(rad);

	return Matrix
	(
		c + (x * x * (1.0f - c)),
		x * y * (1.0f - c) + z * s,
		x * z * (1.0f - c) - y * s,
		0.0f,

		x * y * (1.0f - c) - z * s,
		c + (y * y * (1.0f - c)),
		y * z * (1.0f - c) + x * s,
		0.0f,

		x * z * (1.0f - c) + y * s,
		y * z * (1.0f - c) - x * s,
		c + (z * z * (1.0f - c)),
		0.0f,

		0.0f,
		0.0f,
		0.0f,
		1.0f
	);
}