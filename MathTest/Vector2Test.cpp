#include "stdafx.h"
#include "CppUnitTest.h"
#include <Math/Inc/EngineMath.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace MathTest
{		
	TEST_CLASS(Vector2Test)
	{
	public:
		/*
		TEST_METHOD(Vector2Constructor)
		{
			Engine::Math::Vector2 v{ 1.0f, 2.0f };
			Assert::AreEqual(v.x, 1.0f);
			Assert::AreEqual(v.y, 2.0f);
		}

		TEST_METHOD(Vector2OperatorPlus)
		{
			Engine::Math::Vector2 v0{ 3.0f, 2.0f };
			Engine::Math::Vector2 v1{ 1.0f, 2.0f };
			Engine::Math::Vector2 sum = v0 + v1;
			Assert::AreEqual(sum.x, 4.0f);
			Assert::AreEqual(sum.y, 4.0f);
		}

		TEST_METHOD(Vector2OperatorMinus)
		{
			Engine::Math::Vector2 v0{ 3.0f, 2.0f };
			Engine::Math::Vector2 v1{ 1.0f, 2.0f };
			Engine::Math::Vector2 sum = v0 - v1;
			Assert::AreEqual(sum.x, 2.0f);
			Assert::AreEqual(sum.y, 0.0f);
		}

		TEST_METHOD(Vector2OperatorMultiply)
		{
			Engine::Math::Vector2 v0{ 3.0f, 2.0f };
			Engine::Math::Vector2 v1{ 1.0f, 2.0f };
			Engine::Math::Vector2 sum = v0 * v1;
			Assert::AreEqual(sum.x, 3.0f);
			Assert::AreEqual(sum.y, 4.0f);
		}

		TEST_METHOD(Vector2OperatorDivide)
		{
			Engine::Math::Vector2 v0{ 3.0f, 2.0f };
			Engine::Math::Vector2 sum = v0 / 2;
			Assert::AreEqual(sum.x, 1.5f);
			Assert::AreEqual(sum.y, 1.0f);
		}

		TEST_METHOD(Vector2Dot)
		{
			Engine::Math::Vector2 v0{ 3.0f, 2.0f };
			Engine::Math::Vector2 v1{ 1.0f, 2.0f };
			float dot = Engine::Math::Vector2::Dot(v0, v1);
			Assert::AreEqual(dot, 7.0f);
		}

		TEST_METHOD(Vector2Magnitude)
		{
			Engine::Math::Vector2 v0{ 3.0f, 2.0f };
			float mag = Engine::Math::Vector2::Magnitude(v0);
			Assert::AreEqual(mag, sqrt(13.0f));
		}

		TEST_METHOD(Vector2Add)
		{
			Engine::Math::Vector2 v0{ 3.0f, 2.0f };
			Engine::Math::Vector2 v1{ 1.0f, 2.0f };
			Engine::Math::Vector2 sum = Engine::Math::Vector2::Add(v0,v1);
			Assert::AreEqual(sum.x, 4.0f);
			Assert::AreEqual(sum.y, 4.0f);
		}
		TEST_METHOD(Vector2Subtract)
		{
			Engine::Math::Vector2 v0{ 3.0f, 2.0f };
			Engine::Math::Vector2 v1{ 1.0f, 2.0f };
			Engine::Math::Vector2 sum = Engine::Math::Vector2::Subtract(v0, v1);
			Assert::AreEqual(sum.x, 2.0f);
			Assert::AreEqual(sum.y, 0.0f);
		}

		TEST_METHOD(Vector2Multiply)
		{
			Engine::Math::Vector2 v0{ 3.0f, 2.0f };
			Engine::Math::Vector2 v1{ 1.0f, 2.0f };
			Engine::Math::Vector2 sum = Engine::Math::Vector2::Multiply(v0, v1);
			Assert::AreEqual(sum.x, 3.0f);
			Assert::AreEqual(sum.y, 4.0f);
		}

		TEST_METHOD(Vector2Divide)
		{
			Engine::Math::Vector2 v0{ 3.0f, 2.0f };
			Engine::Math::Vector2 v1{ 1.0f, 2.0f };
			Engine::Math::Vector2 sum = Engine::Math::Vector2::Divide(v0, v1);
			Assert::AreEqual(sum.x, 3.0f);
			Assert::AreEqual(sum.y, 1.0f);
		}
		
*/
	};
}




/* 
Macros - fancy special copy/paste
#define <symbol><<replacement>
#define AREA_CIRCLE(r ((r) * (r) * 3.14f)

float area = AREA_CIRCLE(2 + 1);
float area = (2 + 1) * (2 + 1) *3.14;
*/